<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:premis="http://www.loc.gov/premis/rdf/v1#" 
    xmlns:owl="http://www.w3.org/2002/07/owl#" 
    xmlns:nt="http://www.jcp.org/jcr/nt/1.0" 
    xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
    xmlns:mode="http://www.modeshape.org/1.0" 
    xmlns:oai="http://www.openarchives.org/OAI/2.0/" 
    xmlns:jcr="http://www.jcp.org/jcr/1.0" 
    xmlns:dcterms="http://purl.org/dc/terms/" 
    xmlns:fedoraconfig="http://fedora.info/definitions/v4/config#" 
    xmlns:dcat="http://www.w3.org/ns/dcat#" 
    xmlns:mix="http://www.jcp.org/jcr/mix/1.0" 
    xmlns:foaf="http://xmlns.com/foaf/0.1/" 
    xmlns:image="http://www.modeshape.org/images/1.0" 
    xmlns:sv="http://www.jcp.org/jcr/sv/1.0" 
    xmlns:indexing="http://fedora.info/definitions/v4/indexing#" 
    xmlns:adms="http://www.w3.org/ns/adms#" 
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" 
    xmlns:fedora="http://fedora.info/definitions/v4/repository#" 
    xmlns:rage="http://rageproject.eu/2015/asset/" 
    xmlns:ebucore="http://www.ebu.ch/metadata/ontologies/ebucore/ebucore#" 
    xmlns:ldp="http://www.w3.org/ns/ldp#"  
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:uuid="java:java.util.UUID"
    xmlns:fn="http://www.w3.org/2005/xpath-functions"
    xmlns:java="http://xml.apache.org/xslt/java" 
    xmlns:xalan="http://xml.apache.org/xalan"
    xmlns:xsltc-extension="http://xml.apache.org/xalan/xsltc"
    xmlns:exslt="http://exslt.org/common"
    exclude-result-prefixes="xs math xd fn java uuid xalan xsltc-extension"
    version="1.0">
    
    <xsl:output method="xml" encoding="UTF-8" standalone="yes"
        indent="yes" xalan:indent-amount="3" />
    
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Jan 22, 2016</xd:p>
            <xd:p><xd:b>Author:</xd:b> Atanas Georgiev</xd:p>
            <xd:p></xd:p>
        </xd:desc>
    </xd:doc>
    
    <xsl:param name="userID">
        <xsl:text>bypassAdmin</xsl:text>
    </xsl:param>
    
    <xsl:variable name="dateNow" >
        <xsl:text>2016-01-17T23:31:09.574+02:00</xsl:text>
    </xsl:variable>
    <xsl:variable name="fDateNow">
        <!-- 2016-01-17T23:31:09.574+02:00 -->
       <xsl:value-of select="java:eu.rageproject.util.Utils.currentDateTime()" />
    </xsl:variable>
    
    <xsl:template match="/rage:Asset">
        <xsl:variable name="output">
            <sv:node xmlns:premis="http://www.loc.gov/premis/rdf/v1#" xmlns:owl="http://www.w3.org/2002/07/owl#" xmlns:nt="http://www.jcp.org/jcr/nt/1.0" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:mode="http://www.modeshape.org/1.0" xmlns:oai="http://www.openarchives.org/OAI/2.0/" xmlns:jcr="http://www.jcp.org/jcr/1.0" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:fedoraconfig="http://fedora.info/definitions/v4/config#" xmlns:dcat="http://www.w3.org/ns/dcat#" xmlns:mix="http://www.jcp.org/jcr/mix/1.0" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:image="http://www.modeshape.org/images/1.0" xmlns:sv="http://www.jcp.org/jcr/sv/1.0" xmlns:indexing="http://fedora.info/definitions/v4/indexing#" xmlns:adms="http://www.w3.org/ns/adms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:fedora="http://fedora.info/definitions/v4/repository#" xmlns:rage="http://rageproject.eu/2015/asset/" xmlns:ebucore="http://www.ebu.ch/metadata/ontologies/ebucore/ebucore#" xmlns:ldp="http://www.w3.org/ns/ldp#" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:dc="http://purl.org/dc/elements/1.1/">
                <xsl:variable name="nodeName"><xsl:value-of select="java:eu.rageproject.util.Utils.getUUID('/')"/></xsl:variable>
                <xsl:call-template name="node-common-template">
                	<xsl:with-param name="nodeName"><xsl:value-of select="$nodeName"/></xsl:with-param>
                	<xsl:with-param name="skipMixinType"><xsl:value-of select="true()" /></xsl:with-param>
                </xsl:call-template>
                
                <sv:node>
                	<xsl:call-template name="node-common-template">
                		<xsl:with-param name="nodeName"><xsl:text>metadata</xsl:text></xsl:with-param>
                	</xsl:call-template>
                	 <sv:property sv:name="fedoraconfig:hasOaiRAGERecord" sv:type="String" sv:multiple="true">
                	 	<sv:value><xsl:value-of select="concat('/assets/',$nodeName,'/files/metadata')"/></sv:value>
					 </sv:property>
                	<xsl:call-template name="node-prop-template" />
                	<xsl:call-template name="node-ref-template" />
                </sv:node>
            </sv:node>
        </xsl:variable>
    
        <xsl:apply-templates select="exslt:node-set($output)/*[1]" mode="phase2" />

    </xsl:template>
    
    <xsl:template match="sv:property" mode="phase2">
        <xsl:copy-of select="." />
    </xsl:template>
    
    <xsl:template match="sv:node" mode="phase2">
        <sv:node>
            <xsl:for-each select="./@*">
                <xsl:variable name="attr" select="name(.)" />
                <xsl:attribute name="{$attr}">
                    <xsl:value-of select="."/>
                </xsl:attribute>    
            </xsl:for-each>
            
            <xsl:apply-templates select="./sv:property" mode="phase2"/>
            <xsl:apply-templates select="./sv:node" mode="phase2"/>
        </sv:node>
    </xsl:template>    
    
    <xsl:template match="rage:Classification">
        <xsl:param name="nodeUUID" />
        <xsl:call-template name="node-template">
            <xsl:with-param name="nodeUUID"><xsl:value-of select="$nodeUUID"/></xsl:with-param>
            <xsl:with-param name="nodeName">classification</xsl:with-param>
        </xsl:call-template>
    </xsl:template>
    
    <xsl:template match="rage:Context">
        <xsl:param name="nodeUUID" />
        <xsl:call-template name="node-template">
            <xsl:with-param name="nodeUUID"><xsl:value-of select="$nodeUUID"/></xsl:with-param>
        </xsl:call-template>
    </xsl:template>
    
    <xsl:template match="rage:Solution">
        <xsl:param name="nodeUUID" />
        <xsl:call-template name="node-template">
            <xsl:with-param name="nodeUUID"><xsl:value-of select="$nodeUUID"/></xsl:with-param>
            <xsl:with-param name="nodeName">solution</xsl:with-param>
        </xsl:call-template>
    </xsl:template>
    
    <xsl:template match="rage:Usage">
        <xsl:param name="nodeUUID" />
        <xsl:call-template name="node-template">
            <xsl:with-param name="nodeUUID"><xsl:value-of select="$nodeUUID"/></xsl:with-param>
            <xsl:with-param name="nodeName">usage</xsl:with-param>
        </xsl:call-template>
    </xsl:template>

    <xsl:template match="rage:License">
        <xsl:param name="nodeUUID" />
        <xsl:call-template name="node-template">
            <xsl:with-param name="nodeUUID"><xsl:value-of select="$nodeUUID"/></xsl:with-param>
            <xsl:with-param name="nodeName">license</xsl:with-param>
        </xsl:call-template>
    </xsl:template>

    <xsl:template match="rage:Requirements">
        <xsl:param name="nodeUUID" />
        <xsl:call-template name="node-template">
            <xsl:with-param name="nodeUUID"><xsl:value-of select="$nodeUUID"/></xsl:with-param>
            <xsl:with-param name="nodeName">requirements</xsl:with-param>
        </xsl:call-template>
    </xsl:template>
    
    <xsl:template match="rage:Design">
        <xsl:param name="nodeUUID" />
        <xsl:call-template name="node-template">
            <xsl:with-param name="nodeUUID"><xsl:value-of select="$nodeUUID"/></xsl:with-param>
            <xsl:with-param name="nodeName">design</xsl:with-param>
        </xsl:call-template>
    </xsl:template>    

    <xsl:template match="rage:Implementation">
        <xsl:param name="nodeUUID" />
        <xsl:call-template name="node-template">
            <xsl:with-param name="nodeUUID"><xsl:value-of select="$nodeUUID"/></xsl:with-param>
            <xsl:with-param name="nodeName">implementation</xsl:with-param>
        </xsl:call-template>
    </xsl:template>  

    <xsl:template match="rage:Tests">
        <xsl:param name="nodeUUID" />
        <xsl:call-template name="node-template">
            <xsl:with-param name="nodeUUID"><xsl:value-of select="$nodeUUID"/></xsl:with-param>
            <xsl:with-param name="nodeName">tests</xsl:with-param>
        </xsl:call-template>
    </xsl:template> 

    <xsl:template match="rage:Artefact">
        <xsl:param name="nodeUUID" />
        <xsl:call-template name="node-template">
            <xsl:with-param name="nodeUUID"><xsl:value-of select="$nodeUUID"/></xsl:with-param>
        </xsl:call-template>
    </xsl:template> 
    
    <xsl:template match="rage:CustomMetadata">
        <xsl:param name="nodeUUID" />
        <xsl:call-template name="node-template">
            <xsl:with-param name="nodeUUID"><xsl:value-of select="$nodeUUID"/></xsl:with-param>
        </xsl:call-template>
    </xsl:template>     

    <xsl:template match="dcterms:creator">
        <xsl:param name="nodeUUID" />
        <xsl:apply-templates select="./*">
            <xsl:with-param name="nodeUUID"><xsl:value-of select="$nodeUUID"/></xsl:with-param>
        </xsl:apply-templates>
    </xsl:template>     

    <xsl:template match="dcterms:publisher">
        <xsl:param name="nodeUUID" />
        <xsl:apply-templates select="./*">
            <xsl:with-param name="nodeUUID"><xsl:value-of select="$nodeUUID"/></xsl:with-param>
        </xsl:apply-templates>
    </xsl:template>     

    <xsl:template match="rage:owner">
        <xsl:param name="nodeUUID" />
        <xsl:apply-templates select="./*">
            <xsl:with-param name="nodeUUID"><xsl:value-of select="$nodeUUID"/></xsl:with-param>
        </xsl:apply-templates>
    </xsl:template>   

    <xsl:template match="foaf:Person">
        <xsl:param name="nodeUUID" />
        <xsl:call-template name="node-template">
            <xsl:with-param name="nodeUUID"><xsl:value-of select="$nodeUUID"/></xsl:with-param>
        </xsl:call-template>
    </xsl:template>   

    <xsl:template match="foaf:Organizaton">
        <xsl:param name="nodeUUID" />
        <xsl:call-template name="node-template">
            <xsl:with-param name="nodeUUID"><xsl:value-of select="$nodeUUID"/></xsl:with-param>
        </xsl:call-template>
    </xsl:template>   
    
    <xsl:template match="rage:RelatedAsset">
        <xsl:param name="nodeUUID" />
        <xsl:call-template name="node-template">
            <xsl:with-param name="nodeUUID"><xsl:value-of select="$nodeUUID"/></xsl:with-param>
        </xsl:call-template>
    </xsl:template>       
    
    <xsl:template name="node-common-template">
        <xsl:param name="nodeUUID" />
        <xsl:param name="nodeName" />
        <xsl:param name="skipMixinType" select="false()" />
        
        <xsl:variable name="currentNodeUUID">
            <xsl:choose>
                <xsl:when test="$nodeUUID!=''">
                    <xsl:value-of select="$nodeUUID"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="java:java.util.UUID.randomUUID()"/>                
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        
        <xsl:variable name="currentNodeName">
            <xsl:choose>
                <xsl:when test="$nodeName!=''">
                    <xsl:value-of select="$nodeName"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="java:java.util.UUID.randomUUID()"/>                
                </xsl:otherwise>
            </xsl:choose>                
        </xsl:variable>
        
        <xsl:attribute name="sv:name"><xsl:value-of select="$currentNodeName"/></xsl:attribute>
        <sv:property sv:name="jcr:primaryType" sv:type="Name">
            <sv:value>nt:folder</sv:value>
        </sv:property>
        <sv:property sv:name="jcr:mixinTypes" sv:type="Name" sv:multiple="true">
            <sv:value>fedora:Container</sv:value>
            <sv:value>indexing:Indexable</sv:value>
            <xsl:if test="not($skipMixinType)"><sv:value><xsl:value-of select="name(.)"/></sv:value></xsl:if>
            <sv:value>fedora:Resource</sv:value>            
        </sv:property>
        <sv:property sv:name="jcr:uuid" sv:type="String">
            <sv:value><xsl:value-of select="$currentNodeUUID"/></sv:value>
        </sv:property>
        <sv:property sv:name="jcr:lastModifiedBy" sv:type="String">
            <sv:value><xsl:value-of select="$userID"/></sv:value>
        </sv:property>
        <sv:property sv:name="jcr:createdBy" sv:type="String">
            <sv:value><xsl:value-of select="$userID"/></sv:value>
        </sv:property>
        <sv:property sv:name="jcr:created" sv:type="Date">
            <sv:value><xsl:value-of select="$fDateNow"/></sv:value>
        </sv:property>
        <sv:property sv:name="jcr:lastModified" sv:type="Date">
            <sv:value><xsl:value-of select="$fDateNow"/></sv:value>
        </sv:property>
        <sv:property sv:name="indexing:hasIndexingTransformation" sv:type="String" sv:multiple="true">
            <sv:value>asset</sv:value>
        </sv:property>
        <xsl:variable name="refPrefix">
            <xsl:value-of select="substring-before(name(.), ':')"/>    
        </xsl:variable>
        <xsl:if test="name(.) != 'rage:Asset'">
            <xsl:for-each select="./@*">
                <sv:property>
                    <xsl:attribute name="sv:name"><xsl:value-of select="concat($refPrefix, ':', name(.))"/></xsl:attribute>
                    <xsl:attribute name="sv:type">String</xsl:attribute>
                    <xsl:attribute name="sv:multiple">true</xsl:attribute>
                    <sv:value><xsl:value-of select="."/></sv:value>
                </sv:property>            
            </xsl:for-each>
        </xsl:if>
    </xsl:template>


    <xsl:key name="nodes-by-name" match="*/*[*]" use="concat(generate-id(../.), '|',name(.))"/>
    
    <xsl:template name="node-ref-template">
        <!-- generate WeakReference -->
        <!-- match child nodes containing child nodes -->
   
        <xsl:for-each select="./*[*][count(. | key('nodes-by-name', concat(generate-id(../.), '|',name(.)))[1]) = 1]">
   
            <xsl:variable name="current-grouping-key" select="concat(generate-id(../.), '|',name(.))" />
            <xsl:variable name="current-group" select="key('nodes-by-name', $current-grouping-key)" />
            
            <xsl:variable name="refName">
                <xsl:value-of select="local-name(.)"/>
            </xsl:variable>
            <xsl:variable name="refPrefix">
                <xsl:value-of select="substring-before(name(.), ':')"/>    
            </xsl:variable>
            <!-- generate UUID for groups' nodes -->
            <xsl:variable name="list">
                <xsl:for-each select="exslt:node-set($current-group)">
                    <xsl:element name="refUUID"><xsl:value-of select="java:java.util.UUID.randomUUID()"/></xsl:element>
                </xsl:for-each>
            </xsl:variable>
           
            <sv:property>
                <xsl:attribute name="sv:name">
                    <xsl:value-of select="$refPrefix"/><xsl:text>:</xsl:text><xsl:if test="($refPrefix!='dcterms') and ($refName!='owner')">has</xsl:if><xsl:value-of select="$refName"/><xsl:text>_ref</xsl:text>
                </xsl:attribute>
                <xsl:attribute name="sv:type">WeakReference</xsl:attribute>
                <xsl:attribute name="sv:multiple">true</xsl:attribute>
                <xsl:for-each select="exslt:node-set($list)/refUUID">
                    <sv:value><xsl:value-of select="."/></sv:value>
                </xsl:for-each>
            </sv:property>            
            
            <xsl:for-each select="exslt:node-set($current-group)">
                <xsl:variable name="nth">
                    <xsl:value-of select="position()"/>
                </xsl:variable>
                <xsl:apply-templates select=".">
                    <xsl:with-param name="nodeUUID"><xsl:value-of select="exslt:node-set($list)/refUUID[position()=$nth]"/></xsl:with-param>
                </xsl:apply-templates>                
            </xsl:for-each>
        </xsl:for-each>
        
    </xsl:template>
    
    <xsl:key name="nodes-by-name" match="*/*[not(*)]" use="concat(generate-id(../.), '|',name(.))"/>
    <xsl:template name="node-prop-template">
        <!-- match nodes without childs -->
        
        <xsl:for-each select="./*[not(*)][count(. | key('nodes-by-name', concat(generate-id(../.), '|',name(.)))[1]) = 1]">
            
            <xsl:variable name="current-grouping-key" select="concat(generate-id(../.), '|',name(.))" />
            <xsl:variable name="current-group" select="key('nodes-by-name', $current-grouping-key)" />
       		<xsl:choose>
       			<xsl:when test="name(.)='rage:reference'">
       				<xsl:variable name="ref_path"><xsl:value-of select="."/></xsl:variable>
		            <sv:property sv:name="rage:reference" sv:type="String" sv:multiple="true">
		               <sv:value><xsl:value-of select="$ref_path"/></sv:value>
		            </sv:property>
		            <!-- patch _rage_youtube_ reference bug -->
		            <xsl:if test="not(. ='_rage_youtube_')">
					<sv:property sv:name="rage:hasFile_ref" sv:type="WeakReference" sv:multiple="true">
						<sv:value><xsl:value-of select="java:eu.rageproject.util.Utils.getUUID($ref_path)"/></sv:value>
					</sv:property>
					</xsl:if>
       			</xsl:when>
       			<xsl:otherwise>
       				<xsl:choose>
						<xsl:when test="(name(.)='dcat:accessURL') and not(java:eu.rageproject.util.Utils.isValidURI(./@rdf:resource))"></xsl:when>
       					<xsl:when test="(name(.)='dcat:accessURL') and (starts-with(./@rdf:resource, '/'))"></xsl:when>
       					<xsl:otherwise>
				            <sv:property>
			                <xsl:attribute name="sv:name"><xsl:value-of select="name(.)"/></xsl:attribute>
			                <xsl:attribute name="sv:type">String</xsl:attribute>
			                <xsl:attribute name="sv:multiple">true</xsl:attribute>
			                <xsl:for-each select="$current-group">
			                    <sv:value>
			                        <xsl:choose>
			                            <xsl:when test="@xml:lang"><xsl:value-of select="."/>␞^^␞␞^^␞<xsl:value-of select="@xml:lang"/></xsl:when>
			                            <xsl:when test="@rdf:resource"><xsl:value-of select="@rdf:resource"/>␞^^␞URI</xsl:when>
			                            <xsl:otherwise><xsl:value-of select="."/></xsl:otherwise>
			                        </xsl:choose>
			                    </sv:value>
			                </xsl:for-each>
		            		</sv:property>
       					</xsl:otherwise>
       				</xsl:choose>
	           	</xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>        
    </xsl:template>

    <xsl:template name="node-template">
        
        <xsl:param name="nodeUUID" />
        <xsl:param name="nodeName" />

        <sv:node>
            <!-- generate node common metadata -->
            <xsl:call-template name="node-common-template">
                <xsl:with-param name="nodeName"><xsl:value-of select="$nodeName"/></xsl:with-param>
                <xsl:with-param name="nodeUUID"><xsl:value-of select="$nodeUUID"/></xsl:with-param>
            </xsl:call-template>
            <!-- generate node simple properties metadata -->
            <xsl:call-template name="node-prop-template" />
            <!-- generate node weakreferences -->
            <xsl:call-template name="node-ref-template"/>
        </sv:node>
    </xsl:template>
   
    
    <xsl:template match="text()">
        <!-- nothing -->
    </xsl:template>
    
</xsl:stylesheet>