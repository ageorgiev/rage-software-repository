<?xml version="1.1" encoding="UTF-8"?>
<!--
  A transformation from JCR to RAGE Metadata Model xml (see <http://rageproject.eu/>).
-->

<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" xmlns:ed="http://greenbytes.de/2002/rfcedit"
    xmlns:exslt="http://exslt.org/common" xmlns:nt="http://www.jcp.org/jcr/nt/1.0"
    xmlns:sv="http://www.jcp.org/jcr/sv/1.0" xmlns:jcr="http://www.jcp.org/jcr/1.0"
    xmlns:rage="http://rageproject.eu/2015/asset/" xmlns:dcterms="http://purl.org/dc/terms/"
    xmlns:adms="http://www.w3.org/ns/adms#" xmlns:dcat="http://www.w3.org/ns/dcat#"
    xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:xml="http://www.w3.org/XML/1998/namespace" xmlns:saxon="http://saxon.sf.net/"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:fn="http://www.w3.org/2005/xpath-functions"
    xmlns:xalan="http://xml.apache.org/xalan"
    exclude-result-prefixes="nt ed exslt jcr saxon sv xs math xd xalan fn">

    <xsl:output method="xml" encoding="UTF-8"
        indent="yes" xalan:indent-amount="3" />

    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Feb 3, 2016</xd:p>
            <xd:p><xd:b>Author:</xd:b> Atanas Georgiev, Alexander Grigorov</xd:p>
            <xd:p/>
        </xd:desc>
    </xd:doc>

	<xsl:param name="baseURL" />

    <xsl:template match="/">
        <xsl:apply-templates
            select="//sv:node[sv:property[@sv:name = 'jcr:mixinTypes']/sv:value = 'rage:Asset']"/>
    </xsl:template>

    <xsl:template match="//sv:node[sv:property[@sv:name = 'jcr:mixinTypes']/sv:value = 'rage:Asset']">
        <rage:Asset>
            <xsl:attribute name="xsi:schemaLocation">http://rageproject.eu/2015/asset/ file:DefaultProfile.xsd</xsl:attribute>
            <xsl:attribute name="name">
                <xsl:value-of select="./@sv:name"/>
            </xsl:attribute>
            <xsl:attribute name="url">
                <xsl:for-each select="ancestor::node()">
                    <xsl:value-of select="./@sv:name"/><xsl:text>/</xsl:text>
                </xsl:for-each>
                <xsl:value-of select="./@sv:name" />
            </xsl:attribute>
            <xsl:apply-templates select="./sv:property[@sv:name = 'dcterms:title']/sv:value"/>
            <xsl:apply-templates select="./sv:property[@sv:name = 'dcterms:description']/sv:value"/>
            <xsl:apply-templates select="./sv:property[@sv:name = 'dcterms:type']/sv:value"/>
            <xsl:apply-templates select="./sv:property[@sv:name = 'dcterms:date']/sv:value"/>
            <xsl:apply-templates select="./sv:property[@sv:name = 'dcterms:language']/sv:value"/>

            <xsl:apply-templates select="./sv:property[@sv:name = 'dcterms:creator_ref']/sv:value"/>
            <xsl:apply-templates select="./sv:property[@sv:name = 'dcterms:publisher_ref']/sv:value"/>
            <xsl:apply-templates select="./sv:property[@sv:name = 'rage:owner_ref']/sv:value"/>
            
            <xsl:apply-templates select="./sv:property[@sv:name = 'dcat:keyword']/sv:value"/>
            <xsl:apply-templates select="./sv:property[@sv:name = 'rage:versionInfo']/sv:value"/>
            <xsl:apply-templates select="./sv:property[@sv:name = 'adms:versionNotes']/sv:value"/>
            <xsl:apply-templates select="./sv:property[@sv:name = 'adms:status']/sv:value"/>
            <xsl:apply-templates select="./sv:property[@sv:name = 'rage:maturityLevel']/sv:value"/>
            <xsl:apply-templates select="./sv:property[@sv:name = 'dcat:accessURL']/sv:value"/>
            <xsl:apply-templates select="./sv:property[@sv:name = 'rage:hasCustomMetadata_ref']/sv:value"/>
            <xsl:apply-templates
                select="./sv:node[sv:property[@sv:name = 'jcr:mixinTypes']/sv:value = 'rage:RelatedAsset']"/>
            <xsl:apply-templates
                select="./sv:node[sv:property[@sv:name = 'jcr:mixinTypes']/sv:value = 'rage:Classification']"/>
            <xsl:apply-templates
                select="./sv:node[sv:property[@sv:name = 'jcr:mixinTypes']/sv:value = 'rage:Solution']"/>
            <xsl:apply-templates
                select="./sv:node[sv:property[@sv:name = 'jcr:mixinTypes']/sv:value = 'rage:Usage']"/>
            <xsl:apply-templates
                select="./sv:node[sv:property[@sv:name = 'jcr:mixinTypes']/sv:value = 'rage:License']"
            />
            <xsl:apply-templates
                select="./sv:node[sv:property[@sv:name = 'jcr:mixinTypes']/sv:value = 'rage:Artefact']"
            />            
        </rage:Asset>
    </xsl:template>

    <xsl:template match="sv:node[sv:property[@sv:name = 'jcr:mixinTypes']/sv:value = 'rage:Artefact']">
        <rage:Artefact>
            <xsl:attribute name="name">
                <xsl:value-of select="./sv:property[@sv:name = 'rage:name']/sv:value"/>
            </xsl:attribute>
            <xsl:attribute name="url">
                <xsl:for-each select="ancestor::node()">
                    <xsl:value-of select="./@sv:name"/><xsl:text>/</xsl:text>
                </xsl:for-each>
                <xsl:value-of select="./@sv:name" />
            </xsl:attribute>
            <xsl:attribute name="uuid"><xsl:value-of select="./sv:property[@sv:name = 'rage:hasFile_ref']/sv:value" /></xsl:attribute>           
            <xsl:apply-templates select="./sv:property[@sv:name = 'rage:reference']/sv:value"/>
            <xsl:apply-templates select="./sv:property[@sv:name = 'dcterms:title']/sv:value"/>
            <xsl:apply-templates select="./sv:property[@sv:name = 'dcterms:description']/sv:value"/>

            <xsl:apply-templates select="./sv:property[@sv:name = 'dcterms:type']/sv:value"/>
            <xsl:apply-templates select="./sv:property[@sv:name = 'dcterms:date']/sv:value"/>
            
            <xsl:apply-templates select="./sv:property[@sv:name = 'dcterms:creator_ref']/sv:value"/>
            <xsl:apply-templates select="./sv:property[@sv:name = 'dcterms:publisher_ref']/sv:value"/>
            
            <xsl:apply-templates select="./sv:property[@sv:name = 'rage:versionInfo']/sv:value"/>
            <xsl:apply-templates select="./sv:property[@sv:name = 'dcterms:format']/sv:value"/>
            <xsl:apply-templates select="./sv:property[@sv:name = 'dcat:accessURL']/sv:value"/>
            <xsl:apply-templates select="./sv:property[@sv:name = 'rage:hasFile_ref']/sv:value"/>
            <xsl:apply-templates
                select="./sv:node[sv:property[@sv:name = 'jcr:mixinTypes']/sv:value = 'rage:License']"/>
            <xsl:apply-templates select="./sv:property[@sv:name = 'rage:hasCustomMetadata_ref']/sv:value"/>

        </rage:Artefact>
    </xsl:template>
    
    <xsl:template match="sv:property[@sv:name = 'rage:hasFile_ref']/sv:value">
        <xsl:variable name="val" select="."/>
        <dcat:accessURL>
            <xsl:call-template name="literal-or-resource">
                <xsl:with-param name="value">
                    <xsl:value-of select="$baseURL"/><xsl:text>/assets/</xsl:text><xsl:value-of select="/*/@sv:name"/><xsl:text>/files/</xsl:text><xsl:value-of select="//sv:node[./sv:property/sv:value/text()=$val][@sv:name='jcr:content']/ancestor::*[1]/@sv:name" /><xsl:text>␞^^␞URI</xsl:text>
                </xsl:with-param>
            </xsl:call-template>
        </dcat:accessURL>        
    </xsl:template>
    
    <xsl:template
        match="sv:node[sv:property[@sv:name = 'jcr:mixinTypes']/sv:value = 'rage:Classification']">
        <rage:Classification>
            <xsl:attribute name="url">
                <xsl:for-each select="ancestor::node()">
                    <xsl:value-of select="./@sv:name"/><xsl:text>/</xsl:text>
                </xsl:for-each>
                <xsl:value-of select="./@sv:name" />
            </xsl:attribute>            
            <xsl:apply-templates select="sv:property[@sv:name = 'dcterms:description']/sv:value"/>
            <xsl:apply-templates
                select="//sv:node[sv:property[@sv:name = 'jcr:mixinTypes']/sv:value = 'rage:Context']"
            />
        </rage:Classification>
    </xsl:template>

    <xsl:template
        match="sv:node[sv:property[@sv:name = 'jcr:mixinTypes']/sv:value = 'rage:Context']">
        <rage:Context>
            <xsl:attribute name="name">
                <xsl:value-of select="./sv:property[@sv:name = 'rage:name']/sv:value"/>
            </xsl:attribute>
            <xsl:attribute name="url">
                <xsl:for-each select="ancestor::node()">
                    <xsl:value-of select="./@sv:name"/><xsl:text>/</xsl:text>
                </xsl:for-each>
                <xsl:value-of select="./@sv:name" />
            </xsl:attribute>            
            <xsl:apply-templates select="./sv:property[@sv:name = 'dcterms:title']/sv:value"/>
            <xsl:apply-templates select="./sv:property[@sv:name = 'dcterms:description']/sv:value"/>
            <xsl:apply-templates select="./sv:property[@sv:name = 'dcat:themeTaxonomy']/sv:value"/>
            <xsl:apply-templates select="./sv:property[@sv:name = 'dcat:theme']/sv:value"/>
        </rage:Context>
    </xsl:template>

    <xsl:template
        match="sv:node[sv:property[@sv:name = 'jcr:mixinTypes']/sv:value = 'rage:Solution']">
        <rage:Solution>
            <xsl:attribute name="url">
                <xsl:for-each select="ancestor::node()">
                    <xsl:value-of select="./@sv:name"/><xsl:text>/</xsl:text>
                </xsl:for-each>
                <xsl:value-of select="./@sv:name" />
            </xsl:attribute>            
            <xsl:apply-templates select="sv:property[@sv:name = 'dcterms:description']/sv:value"/>
            <xsl:apply-templates
                select="./sv:node[sv:property[@sv:name = 'jcr:mixinTypes']/sv:value = 'rage:Requirements']"/>
            <xsl:apply-templates
                select="./sv:node[sv:property[@sv:name = 'jcr:mixinTypes']/sv:value = 'rage:Design']"/>
            <xsl:apply-templates
                select="./sv:node[sv:property[@sv:name = 'jcr:mixinTypes']/sv:value = 'rage:Implementation']"/>
            <xsl:apply-templates
                select="./sv:node[sv:property[@sv:name = 'jcr:mixinTypes']/sv:value = 'rage:Tests']"
            />
        </rage:Solution>
    </xsl:template>

    <xsl:template
        match="sv:node[sv:property[@sv:name = 'jcr:mixinTypes']/sv:value = 'rage:Requirements']">
        <rage:Requirements>
            <xsl:attribute name="url">
                <xsl:for-each select="ancestor::node()">
                    <xsl:value-of select="./@sv:name"/><xsl:text>/</xsl:text>
                </xsl:for-each>
                <xsl:value-of select="./@sv:name" />
            </xsl:attribute>            
            <xsl:apply-templates select="sv:property[@sv:name = 'dcterms:description']/sv:value"/>
            <xsl:apply-templates select="./sv:property[@sv:name = 'rage:hasArtefact_ref']/sv:value"/>
        </rage:Requirements>
    </xsl:template>

    <xsl:template match="sv:node[sv:property[@sv:name = 'jcr:mixinTypes']/sv:value = 'rage:Design']">
        <rage:Design>
            <xsl:attribute name="url">
                <xsl:for-each select="ancestor::node()">
                    <xsl:value-of select="./@sv:name"/><xsl:text>/</xsl:text>
                </xsl:for-each>
                <xsl:value-of select="./@sv:name" />
            </xsl:attribute>            
            <xsl:apply-templates select="sv:property[@sv:name = 'dcterms:description']/sv:value"/>
            <xsl:apply-templates select="./sv:property[@sv:name = 'rage:hasArtefact_ref']/sv:value"/>
        </rage:Design>
    </xsl:template>

    <xsl:template match="sv:node[sv:property[@sv:name = 'jcr:mixinTypes']/sv:value = 'rage:Implementation']">
        <rage:Implementation>
            <xsl:attribute name="url">
                <xsl:for-each select="ancestor::node()">
                    <xsl:value-of select="./@sv:name"/><xsl:text>/</xsl:text>
                </xsl:for-each>
                <xsl:value-of select="./@sv:name" />
            </xsl:attribute>            
            <xsl:apply-templates select="sv:property[@sv:name = 'dcterms:description']/sv:value"/>
            <xsl:apply-templates select="sv:property[@sv:name = 'rage:gameEngine']/sv:value"/>
            <xsl:apply-templates select="sv:property[@sv:name = 'rage:gamePlatform']/sv:value"/>
            <xsl:apply-templates select="sv:property[@sv:name = 'rage:progLanguage']/sv:value"/>
            <xsl:apply-templates select="sv:property[@sv:name = 'rage:hasCustomMetadata_ref']/sv:value"/>
            <xsl:apply-templates select="./sv:property[@sv:name = 'rage:hasArtefact_ref']/sv:value"/>
        </rage:Implementation>
    </xsl:template>

    <xsl:template match="sv:node[sv:property[@sv:name = 'jcr:mixinTypes']/sv:value = 'rage:Tests']">
        <rage:Tests>
            <xsl:attribute name="url">
                <xsl:for-each select="ancestor::node()">
                    <xsl:value-of select="./@sv:name"/><xsl:text>/</xsl:text>
                </xsl:for-each>
                <xsl:value-of select="./@sv:name" />
            </xsl:attribute>            
            <xsl:apply-templates select="sv:property[@sv:name = 'dcterms:description']/sv:value"/>
            <xsl:apply-templates select="./sv:property[@sv:name = 'rage:hasArtefact_ref']/sv:value"/>
        </rage:Tests>
    </xsl:template>

    <xsl:template match="sv:node[sv:property[@sv:name = 'jcr:mixinTypes']/sv:value = 'rage:Usage']">
        <rage:Usage>
            <xsl:attribute name="url">
                <xsl:for-each select="ancestor::node()">
                    <xsl:value-of select="./@sv:name"/><xsl:text>/</xsl:text>
                </xsl:for-each>
                <xsl:value-of select="./@sv:name" />
            </xsl:attribute>            
            <xsl:apply-templates select="sv:property[@sv:name = 'dcterms:description']/sv:value"/>
            <xsl:apply-templates select="./sv:property[@sv:name = 'rage:hasArtefact_ref']/sv:value"/>
        </rage:Usage>
    </xsl:template>

    <xsl:template
        match="sv:node[sv:property[@sv:name = 'jcr:mixinTypes']/sv:value = 'rage:License']">
        <rage:License>
            <xsl:attribute name="url">
                <xsl:for-each select="ancestor::node()">
                    <xsl:value-of select="./@sv:name"/><xsl:text>/</xsl:text>
                </xsl:for-each>
                <xsl:value-of select="./@sv:name" />
            </xsl:attribute>
            <xsl:apply-templates select="sv:property[@sv:name = 'dcterms:title']/sv:value"/>
            <xsl:apply-templates select="sv:property[@sv:name = 'dcterms:description']/sv:value"/>
            <xsl:apply-templates select="sv:property[@sv:name = 'dcterms:type']/sv:value"/>
            <xsl:apply-templates select="sv:property[@sv:name = 'dcat:accessURL']/sv:value"/>
        </rage:License>
    </xsl:template>

    <xsl:template
        match="sv:node[sv:property[@sv:name = 'jcr:mixinTypes']/sv:value = 'rage:RelatedAsset']">
        <rage:RelatedAsset>            
            <xsl:attribute name="name">
                <xsl:value-of select="./sv:property[@sv:name = 'rage:name']/sv:value"/>
            </xsl:attribute>            
            <xsl:attribute name="minVersion">
                <xsl:value-of select="./sv:property[@sv:name = 'rage:minVersion']/sv:value"/>
            </xsl:attribute>
            <xsl:attribute name="maxVersion">
                <xsl:value-of select="./sv:property[@sv:name = 'rage:maxVersion']/sv:value"/>
            </xsl:attribute>
            <xsl:attribute name="url">
                <xsl:for-each select="ancestor::node()">
                    <xsl:value-of select="./@sv:name"/><xsl:text>/</xsl:text>
                </xsl:for-each>
                <xsl:value-of select="./@sv:name" />
            </xsl:attribute>            
            <xsl:apply-templates select="sv:property[@sv:name = 'dcterms:description']/sv:value"/>
            <rage:relationType>
                <xsl:value-of select="./sv:property[@sv:name = 'rage:relationType']/sv:value"/>
            </rage:relationType>
            <!-- 
                // TODO: dcat:accessURL
            -->
        </rage:RelatedAsset>
    </xsl:template>

    <xsl:template match="sv:property[@sv:name = 'dcterms:description']/sv:value">
        <dcterms:description xml:lang="en">
            <xsl:call-template name="literal-or-resource">
                <xsl:with-param name="value">
                    <xsl:value-of select="."/>
                </xsl:with-param>
            </xsl:call-template>
        </dcterms:description>
    </xsl:template>

    <xsl:template match="sv:property[@sv:name = 'dcterms:title']/sv:value">
        <dcterms:title xml:lang="en">
            <xsl:call-template name="literal-or-resource">
                <xsl:with-param name="value">
                    <xsl:value-of select="."/>
                </xsl:with-param>
            </xsl:call-template>
            <!--
            <xsl:value-of select="." />
            -->
            <!-- 
            <xsl:value-of select="saxon:base64Binary-to-string(., 'UTF8')"/>
             -->
            <!--
                    <xsl:value-of select="saxon:base64Binary-to-string(xs:base64Binary('QW5vdGhlciBBc3NldCB2ZXJzaW9uIDEuMi0xLjggaXMgcmVxdWlyZWQuGF5eGBheXhhlbg=='), 'UTF8')"/>
                        -->
        </dcterms:title>
    </xsl:template>

    <xsl:template match="sv:property[@sv:name = 'dcterms:type']/sv:value">
        <dcterms:type>
            <xsl:call-template name="literal-or-resource">
                <xsl:with-param name="value">
                    <xsl:value-of select="."/>
                </xsl:with-param>
            </xsl:call-template>
        </dcterms:type>
    </xsl:template>

    <xsl:template match="sv:property[@sv:name = 'rage:gameEngine']/sv:value">
        <rage:gameEngine>
            <xsl:call-template name="literal-or-resource">
                <xsl:with-param name="value">
                    <xsl:value-of select="."/>
                </xsl:with-param>
            </xsl:call-template>
        </rage:gameEngine>
    </xsl:template>

    <xsl:template match="sv:property[@sv:name = 'rage:gamePlatform']/sv:value">
        <rage:gamePlatform>
            <xsl:call-template name="literal-or-resource">
                <xsl:with-param name="value">
                    <xsl:value-of select="."/>
                </xsl:with-param>
            </xsl:call-template>
        </rage:gamePlatform>
    </xsl:template>

    <xsl:template match="sv:property[@sv:name = 'rage:progLanguage']/sv:value">
        <rage:progLanguage>
            <xsl:call-template name="literal-or-resource">
                <xsl:with-param name="value">
                    <xsl:value-of select="."/>
                </xsl:with-param>
            </xsl:call-template>
        </rage:progLanguage>
    </xsl:template>
    
    <xsl:template match="sv:property[@sv:name = 'dcterms:date']/sv:value">
        <dcterms:date>
            <xsl:value-of select="."/>
        </dcterms:date>
    </xsl:template>
    
    <xsl:template match="sv:property[@sv:name = 'rage:reference']/sv:value">
        <rage:reference>
            <xsl:value-of select="."/>
        </rage:reference>
    </xsl:template>
    
    <xsl:template match="sv:property[@sv:name = 'dcterms:language']/sv:value">
        <dcterms:language>
            <xsl:call-template name="literal-or-resource">
                <xsl:with-param name="value">
                    <xsl:value-of select="."/>
                </xsl:with-param>
            </xsl:call-template>        
        </dcterms:language>
    </xsl:template>

    <xsl:template match="sv:property[@sv:name = 'rage:hasCustomMetadata_ref']/sv:value">
        <xsl:variable name="uuid"><xsl:value-of select="."/></xsl:variable>
        <rage:CustomMetadata>
            <xsl:attribute name="url">
                <xsl:for-each select="ancestor::node()">
                    <xsl:value-of select="./@sv:name"/><xsl:text>/</xsl:text>
                </xsl:for-each>
                <xsl:value-of select="./@sv:name" />
            </xsl:attribute>            
            <xsl:apply-templates select="//sv:node[sv:property[@sv:name = 'jcr:uuid']/sv:value = $uuid]/sv:property[@sv:name = 'rage:name']"/>
            <xsl:apply-templates select="//sv:node[sv:property[@sv:name = 'jcr:uuid']/sv:value = $uuid]/sv:property[@sv:name = 'rage:value']"/>
        </rage:CustomMetadata>
    </xsl:template>
    
    <xsl:template match="sv:property[@sv:name = 'rage:hasArtefact_ref']/sv:value">
        <xsl:variable name="uuid"><xsl:value-of select="."/></xsl:variable>
        <xsl:apply-templates select="//sv:node[sv:property[@sv:name = 'jcr:uuid']/sv:value = $uuid]"/>
    </xsl:template>
    
    <xsl:template match="sv:property[@sv:name = 'rage:name']/sv:value">
        <rage:name>
            <xsl:value-of select="."/>
        </rage:name>
    </xsl:template>
    
    <xsl:template match="sv:property[@sv:name = 'rage:value']/sv:value">
        <rage:value>
            <xsl:value-of select="."/>
        </rage:value>
    </xsl:template>
    
    <xsl:template match="sv:property[@sv:name = 'dcterms:format']/sv:value">
        <dcterms:format>
            <xsl:value-of select="."/>
        </dcterms:format>
    </xsl:template>
    
    <xsl:template match="sv:property[@sv:name = 'dcterms:creator_ref']/sv:value">
        <xsl:variable name="uuid"><xsl:value-of select="."/></xsl:variable>
        <dcterms:creator>
            <xsl:apply-templates select="//sv:node[sv:property[@sv:name = 'jcr:uuid']/sv:value = $uuid]"/>
        </dcterms:creator>
    </xsl:template>
    
    <xsl:template match="sv:property[@sv:name = 'dcterms:publisher_ref']/sv:value">
        <xsl:variable name="uuid"><xsl:value-of select="."/></xsl:variable>
        <dcterms:publisher>
            <xsl:apply-templates select="//sv:node[sv:property[@sv:name = 'jcr:uuid']/sv:value = $uuid]"/>
        </dcterms:publisher>
    </xsl:template>
    
    <xsl:template match="sv:property[@sv:name = 'rage:owner_ref']/sv:value">
        <xsl:variable name="uuid"><xsl:value-of select="."/></xsl:variable>
        <rage:owner>
            <xsl:apply-templates select="//sv:node[sv:property[@sv:name = 'jcr:uuid']/sv:value = $uuid]"/>
        </rage:owner>
    </xsl:template>
    
    <xsl:template match="sv:node[sv:property[@sv:name = 'jcr:mixinTypes']/sv:value = 'foaf:Person']">
        <foaf:Person>
            <xsl:attribute name="url">
                <xsl:for-each select="ancestor::node()">
                    <xsl:value-of select="./@sv:name"/><xsl:text>/</xsl:text>
                </xsl:for-each>
                <xsl:value-of select="./@sv:name" />
            </xsl:attribute>            
            <xsl:apply-templates select="sv:property[@sv:name = 'foaf:name']/sv:value"/>
            <xsl:apply-templates select="sv:property[@sv:name = 'foaf:mbox']/sv:value"/>
            <xsl:apply-templates select="sv:property[@sv:name = 'foaf:homepage']/sv:value"/>
        </foaf:Person>
    </xsl:template>

    <xsl:template match="sv:property[@sv:name = 'foaf:name']/sv:value">
        <foaf:name>
            <xsl:call-template name="literal-or-resource">
                <xsl:with-param name="value">
                    <xsl:value-of select="."/>
                </xsl:with-param>
            </xsl:call-template>
        </foaf:name>
    </xsl:template>
    
    <xsl:template match="sv:property[@sv:name = 'foaf:mbox']/sv:value">
        <foaf:mbox>
            <xsl:call-template name="literal-or-resource">
                <xsl:with-param name="value">
                    <xsl:value-of select="."/>
                </xsl:with-param>
            </xsl:call-template>
        </foaf:mbox>
    </xsl:template>
    
    <xsl:template match="sv:property[@sv:name = 'foaf:homepage']/sv:value">
        <foaf:homepage>
            <xsl:call-template name="literal-or-resource">
                <xsl:with-param name="value">
                    <xsl:value-of select="."/>
                </xsl:with-param>
            </xsl:call-template>
        </foaf:homepage>
    </xsl:template>
    
    <xsl:template match="sv:node[sv:property[@sv:name = 'jcr:mixinTypes']/sv:value = 'foaf:Organizaton']">
        <foaf:Organizaton>
            <xsl:attribute name="url">
                <xsl:for-each select="ancestor::node()">
                    <xsl:value-of select="./@sv:name"/><xsl:text>/</xsl:text>
                </xsl:for-each>
                <xsl:value-of select="./@sv:name" />
            </xsl:attribute>            
            <xsl:apply-templates select="sv:property[@sv:name = 'foaf:name']/sv:value"/>
            <xsl:apply-templates select="sv:property[@sv:name = 'foaf:mbox']/sv:value"/>
            <xsl:apply-templates select="sv:property[@sv:name = 'foaf:homepage']/sv:value"/>
        </foaf:Organizaton>
    </xsl:template>
    
    <xsl:template match="sv:property[@sv:name = 'dcat:keyword']/sv:value">
        <dcat:keyword>
            <xsl:call-template name="literal-or-resource">
                <xsl:with-param name="value">
                    <xsl:value-of select="."/>
                </xsl:with-param>
            </xsl:call-template>
        </dcat:keyword>
    </xsl:template>

    <xsl:template match="sv:property[@sv:name = 'rage:versionInfo']/sv:value">
        <rage:versionInfo>
            <xsl:call-template name="literal-or-resource">
                <xsl:with-param name="value">
                    <xsl:value-of select="."/>
                </xsl:with-param>
            </xsl:call-template>
        </rage:versionInfo>
    </xsl:template>

    <xsl:template match="sv:property[@sv:name = 'adms:versionNotes']/sv:value">
        <adms:versionNotes>
            <xsl:call-template name="literal-or-resource">
                <xsl:with-param name="value">
                    <xsl:value-of select="."/>
                </xsl:with-param>
            </xsl:call-template>
        </adms:versionNotes>
    </xsl:template>

    <xsl:template match="sv:property[@sv:name = 'adms:status']/sv:value">
        <adms:status>
            <xsl:call-template name="literal-or-resource">
                <xsl:with-param name="value">
                    <xsl:value-of select="."/>
                </xsl:with-param>
            </xsl:call-template>
        </adms:status>
    </xsl:template>

    <xsl:template match="sv:property[@sv:name = 'rage:maturityLevel']/sv:value">
        <rage:maturityLevel>
        <xsl:value-of select="."/>
        </rage:maturityLevel>
    </xsl:template>

    <xsl:template match="sv:property[@sv:name = 'dcat:accessURL']/sv:value">
        <dcat:accessURL>
            <xsl:call-template name="literal-or-resource">
                <xsl:with-param name="value">
                    <xsl:value-of select="."/>
                </xsl:with-param>
            </xsl:call-template>
        </dcat:accessURL>
    </xsl:template>

    <xsl:template match="sv:property[@sv:name = 'dcat:themeTaxonomy']/sv:value">
        <dcat:themeTaxonomy>
            <xsl:call-template name="literal-or-resource">
                <xsl:with-param name="value">
                    <xsl:value-of select="."/>
                </xsl:with-param>
            </xsl:call-template>
        </dcat:themeTaxonomy>
    </xsl:template>

    <xsl:template match="sv:property[@sv:name = 'dcat:theme']/sv:value">
        <dcat:theme>
            <xsl:call-template name="literal-or-resource">
                <xsl:with-param name="value">
                    <xsl:value-of select="."/>
                </xsl:with-param>
            </xsl:call-template>
        </dcat:theme>
    </xsl:template>
        
    <xsl:template name="literal-or-resource">
        <xsl:param name="value"/>
        <xsl:choose>
            <xsl:when test="contains($value, '␞^^␞␞^^␞')">
                <xsl:attribute name="xml:lang">
                    <xsl:value-of select="substring-after($value, '␞^^␞␞^^␞')"/>
                </xsl:attribute>
                <xsl:value-of select="substring-before($value, '␞^^␞␞^^␞')"/>
            </xsl:when>
            <xsl:when test="contains($value, '␞^^␞URI')">
                <xsl:attribute name="rdf:resource">
                    <xsl:value-of select="substring-before($value, '␞^^␞URI')"/>
                </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$value"/>
            </xsl:otherwise>
        </xsl:choose>

    </xsl:template>

    <xsl:template match="text()"> </xsl:template>
</xsl:stylesheet>