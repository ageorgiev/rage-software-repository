/**
 * Copyright (C) 2016-2017 Sofia University (https://www.uni-sofia.bg/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.rageproject.security;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jasig.cas.client.proxy.ProxyGrantingTicketStorage;
import org.jasig.cas.client.proxy.ProxyGrantingTicketStorageImpl;
import org.jasig.cas.client.validation.Cas20ProxyTicketValidator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.cas.ServiceProperties;
import org.springframework.security.cas.authentication.CasAssertionAuthenticationToken;
import org.springframework.security.cas.authentication.CasAuthenticationProvider;
import org.springframework.security.cas.web.CasAuthenticationEntryPoint;
import org.springframework.security.cas.web.CasAuthenticationFilter;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;
import org.springframework.security.web.authentication.logout.LogoutFilter;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.WebUtils;

@Configuration
@Order(6)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Value("${security.cas.service.url}")
	private String securityCasServiceUrl;	
	
	@Value("${security.cas.proxy.service.url}")
	private String securityCasProxyServiceUrl;
	
	@Value("${security.cas.proxy.callback.url}")
	private String securityCasProxyCallbackUrl;
	
	@Value("${security.cas.login.url}")
	private String securityCasLoginUrl;	
	
	@Value("${security.cas.logout.url}")
	private String securityCasLogoutUrl;		
	
    @Bean
    public ServiceProperties serviceProperties() {
        final ServiceProperties serviceProperties = new ServiceProperties();
        serviceProperties.setService(securityCasServiceUrl);
        serviceProperties.setSendRenew(false);
        
        return serviceProperties;
    }

    @Bean
    public CasAuthenticationProvider casAuthenticationProvider() {
        CasAuthenticationProvider casAuthenticationProvider = new CasAuthenticationProvider();
        casAuthenticationProvider.setAuthenticationUserDetailsService(authenticationUserDetailsService());
        casAuthenticationProvider.setServiceProperties(serviceProperties());
        casAuthenticationProvider.setTicketValidator(cas20ProxyServiceTicketValidator());
        casAuthenticationProvider.setKey("an_id_for_this_auth_provider_only");
        return casAuthenticationProvider;
    }
	
    @Bean
    public AuthenticationUserDetailsService<CasAssertionAuthenticationToken> authenticationUserDetailsService() {
        return new CasAuthenticationUserDetailsService();
    }

    @Bean
    public ProxyGrantingTicketStorage proxyGrantingTicketStorage() {
    	return new ProxyGrantingTicketStorageImpl();
    }
    
    @Bean
    public Cas20ProxyTicketValidator cas20ProxyServiceTicketValidator() {
    	final Cas20ProxyTicketValidator cas20ProxyTicketValidator = new Cas20ProxyTicketValidator(securityCasProxyServiceUrl);
    	cas20ProxyTicketValidator.setAcceptAnyProxy(true);
    	cas20ProxyTicketValidator.setProxyCallbackUrl(securityCasProxyCallbackUrl);
    	cas20ProxyTicketValidator.setProxyGrantingTicketStorage(proxyGrantingTicketStorage());
        return cas20ProxyTicketValidator;
    }    
    
    @Bean
    public CasAuthenticationFilter casAuthenticationFilter() throws Exception {
        CasAuthenticationFilter casAuthenticationFilter = new CasAuthenticationFilter();
        casAuthenticationFilter.setProxyGrantingTicketStorage(proxyGrantingTicketStorage());
        casAuthenticationFilter.setProxyReceptorUrl("/j_spring_cas_security_proxyreceptor");
        casAuthenticationFilter.setAuthenticationManager(authenticationManager());
        return casAuthenticationFilter;
    }

    @Bean
    public CasAuthenticationEntryPoint casAuthenticationEntryPoint() {
        CasAuthenticationEntryPoint casAuthenticationEntryPoint = new CasAuthenticationEntryPoint();
        casAuthenticationEntryPoint.setLoginUrl(securityCasLoginUrl);
        casAuthenticationEntryPoint.setServiceProperties(serviceProperties());
        return casAuthenticationEntryPoint;
    }
    
    private LogoutFilter logoutFilter() throws IOException {
    	LogoutFilter filter = new LogoutFilter(securityCasLogoutUrl, new SecurityContextLogoutHandler());
    	filter.setFilterProcessesUrl("/j_spring_security_logout");
    	return filter;  	
    }        

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
        	.csrf().disable()
            .authorizeRequests()
            	.antMatchers("/", "/login**", "/webjars/**").permitAll()
                .antMatchers("/resources/**").permitAll()
                .antMatchers("/xsd/**").permitAll()
                .antMatchers("/assets/ping").authenticated()
                .antMatchers("/assets/search").authenticated()
                .antMatchers("/assets/search/**").authenticated()
                .antMatchers(HttpMethod.GET, "/assets/{^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$}").access("isAuthenticated() or hasIpAddress('xxx.xxx.xxx.xxx/32')")
                .antMatchers("/assets/**").authenticated()
                .antMatchers("/webapp/**").authenticated()
                .antMatchers("/oai/**").access("hasIpAddress('xxx.xxx.xxx.xxx/32')")
                .antMatchers("/assets/upload/**").authenticated()
                .antMatchers(HttpMethod.GET, "/taxonomies/**").permitAll()
                .antMatchers(HttpMethod.POST, "/taxonomies/**").access("hasIpAddress('xxx.xxx.xxx.xxx/32')")
                .antMatchers(HttpMethod.PUT, "/taxonomies/**").access("hasIpAddress('xxx.xxx.xxx.xxx/32')")
                .antMatchers(HttpMethod.DELETE, "/taxonomies/**").access("hasIpAddress('xxx.xxx.xxx.xxx/32')")
                .anyRequest().authenticated()
                .and().exceptionHandling().authenticationEntryPoint(new LoginUrlAuthenticationEntryPoint("/"))
    			.and().logout().logoutSuccessUrl("/").permitAll();
        
        	http.addFilter(logoutFilter());
        	http.addFilter(casAuthenticationFilter());
        	http
            .exceptionHandling()
                .authenticationEntryPoint(casAuthenticationEntryPoint());
    }
    
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
            .authenticationProvider(casAuthenticationProvider());
    }    
    
    @SuppressWarnings("unused")
	private Filter csrfHeaderFilter() {
  
		return new OncePerRequestFilter() {
			@Override
			protected void doFilterInternal(HttpServletRequest request,
					HttpServletResponse response, FilterChain filterChain)
					throws ServletException, IOException {
				CsrfToken csrf = (CsrfToken) request.getAttribute(CsrfToken.class
						.getName());
				if (csrf != null) {
					Cookie cookie = WebUtils.getCookie(request, "XSRF-TOKEN");
					String token = csrf.getToken();
					if (cookie == null 
							|| token != null && !token.equals(cookie.getValue())) {
						cookie = new Cookie("XSRF-TOKEN", token);
						cookie.setPath("/");
						response.addCookie(cookie);
					}
				}
				filterChain.doFilter(request, response);
			}
		};
	}

	@SuppressWarnings("unused")
	private CsrfTokenRepository csrfTokenRepository() {
		HttpSessionCsrfTokenRepository repository = new HttpSessionCsrfTokenRepository();
		repository.setHeaderName("X-XSRF-TOKEN");
		return repository;
	}
}