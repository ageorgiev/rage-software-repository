/**
 * Copyright (C) 2016-2017 Sofia University (https://www.uni-sofia.bg/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.rageproject.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.URI;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.core.io.ResourceLoader;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import eu.rageproject.model.Asset;
import eu.rageproject.model.jcr.FolderNodeHelper;
import eu.rageproject.model.jcr.Node;
import eu.rageproject.model.jcr.NtResourceNodeHelper;
import eu.rageproject.repository.FedoraRepository;
import eu.rageproject.repository.RepositoryException;
import eu.rageproject.storage.FileStorageService;
import eu.rageproject.util.ListFilesUtil;
import eu.rageproject.util.Utils;

@EnableConfigurationProperties
@Service
public class AssetService {

	private final static String ASSET_EMPTY_FILE = "empty.zip";
	public final static String METADATA_FILE = "metadata.xml";
	public final static String INDEXING_TRANSFORMATION = "asset";

	@Value("${multipart.maxFileSize}")
	int maxFileSize;

	@Value("${server.base.url}")
	private String baseURL;

	@Autowired
	NtResourceNodeHelper artefact;

	@Autowired
	private ResourceLoader resourceLoader;
	
	private final FileStorageService storageService;
	private final FedoraRepository repository;
	private final TransformService transformService;

	@Autowired
	public AssetService(FileStorageService storageService, FedoraRepository repository,
			TransformService transformService) {
		this.storageService = storageService;
		this.repository = repository;
		this.transformService = transformService;
	}

	public void copy(UUID sourceUUID, UUID destUUID) throws Exception {
		repository.copy("/" + sourceUUID, "/" + destUUID);
		storageService.copy(sourceUUID + ".zip", destUUID + ".zip");
	}

	public String create() throws Exception {
		File file = resourceLoader.getResource("classpath:" + ASSET_EMPTY_FILE).getFile();
		FileInputStream input = new FileInputStream(file);
		MultipartFile multipartFile = new MockMultipartFile("file", file.getName(), "application/zip",
				IOUtils.toByteArray(input));
		upload(multipartFile);
		return Utils.getUUID("/");
	}

	public void delete(UUID uuid) throws Exception {
		repository.beginTransaction();
		try {
			repository.delete("/" + uuid);
			//TODO delete asset package
			storageService.delete(uuid + ".zip");
			repository.commitTransaction();
		} catch (Exception e) {
			repository.rollbackTransaction();
			throw new RepositoryException(e.getMessage(), e);
		}
	}

	public String fetchXml(UUID uuid) throws Exception {
		String content = repository.exportJcr("/" + uuid);
		File file = storageService.writeToTempFile(content);

		Map<String, Object> transformationParameters = new HashMap<String, Object>();
		transformationParameters.put(TransformService.PARAM_ASSET_UUID, uuid);
		transformationParameters.put("baseURL", baseURL);
		String xml = transformService.process(file.getAbsolutePath(), "jcr2rage.xsl", transformationParameters);

		file.delete();

		return xml;
	}

	/**
	 * 
	 * @param resourcePath
	 * @param data
	 *            {"GitHubProfile#4271293":["admin"],"EVERYONE":["reader"]}
	 * @throws Exception
	 */
	public void setAccessRoles(String resourcePath, Map<String, List<String>> data) throws Exception {

		repository.setAccessRoles(resourcePath, data);

		String ns = "PREFIX rage: <http://rageproject.eu/2015/asset/>\r\n";

		StringBuilder deleteQuery = (new StringBuilder(ns))
				.append("DELETE { <> rage:adminaccess ?adminaccess .\r\n" + " <> rage:readaccess ?readaccess}"
						+ " WHERE {<> rage:adminaccess ?adminaccess .\r\n " + " <> rage:readaccess ?readaccess }");

		StringBuilder insertQuery = (new StringBuilder(ns)).append("INSERT { \r\n");
		data.forEach((user, roles) -> {
			roles.forEach((role) -> {
				if (role.equals("admin")) {
					insertQuery.append("<> rage:adminaccess \"").append(user).append("\" . \r\n");
				} else if (role.equals("reader")) {
					insertQuery.append("<> rage:readaccess \"").append(user).append("\" . \r\n");
				}
			});
		});
		insertQuery.append("} \r\n");
		insertQuery.append("WHERE {} \r\n");

		repository.query(resourcePath, deleteQuery.toString());
		repository.query(resourcePath, insertQuery.toString());
	}

	public void processAssetPackage(File zipFile, File jcrFile) throws Exception {

		String path = null;
		ZipInputStream zipInput = null;
		FileOutputStream fos = null;
		FileInputStream fInput = null;

		try {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			String userName = auth.getName();

			Node folderNode = FolderNodeHelper.getNode(userName, INDEXING_TRANSFORMATION);

			String destination = zipFile.getAbsoluteFile().getParentFile().getAbsolutePath()
					+ File.separator + FilenameUtils.getBaseName(zipFile.getName());

			File directory = new File(destination);

			if (!directory.exists()) {
				directory.mkdirs();
			} else {
				throw new Exception("Faild extracting package file.");
			}

			URI basePath = directory.toURI();

			byte[] buffer = new byte[2048];

			fInput = new FileInputStream(zipFile);
			zipInput = new ZipInputStream(fInput);

			ZipEntry entry = zipInput.getNextEntry();

			while (entry != null) {
				String entryName = entry.getName();
				File file = new File(destination + File.separator + entryName);

				if (entry.isDirectory()) {
					File newDir = new File(file.getAbsolutePath());
					if (!newDir.exists()) {
						boolean success = newDir.mkdirs();
						if (success == false) {
							throw new Exception("Faild extracting package file.");
						}
					}
				} else {
					FileOutputStream fOutput = new FileOutputStream(file);
					int count = 0;
					while ((count = zipInput.read(buffer)) > 0) {
						fOutput.write(buffer, 0, count);
					}
					fOutput.close();

					artefact.setUserName(userName);
					artefact.setFileName(file.getName());
					artefact.setHasSize(file.length());
					artefact.setMimeType(ListFilesUtil.guessFileType(file));
					path = basePath.relativize(file.toURI()).getPath();
					artefact.setPath(path);
					artefact.setUuid(Utils.randomUUID(path));
					artefact.setSha1(Utils.sha1(file));
					artefact.setBase64FilePipe(file);

					folderNode.getNodeOrProperty().add(artefact.getNode());
				}
				
				zipInput.closeEntry();
				entry = zipInput.getNextEntry();
			}

			fos = new FileOutputStream(jcrFile);
			JAXBContext jaxbContext = JAXBContext.newInstance(Node.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(folderNode, fos);

		} catch (Exception e) {
			try {
				zipFile.delete();
			} catch (Exception ex) {
			}
			throw new Exception(e);
		} finally {
			try {
				zipInput.closeEntry();
				zipInput.close();
				fInput.close();
				fos.close();
			} catch (Exception e) {
			}
		}
	}

	public void update(UUID uuid, Asset asset) throws Exception {

		File xmlFile = null;
		File jcrFile = null;

		repository.beginTransaction();
		try {
			xmlFile = storageService.writeToTempFile(asset.getMetadata());

			Map<String, Object> transformationParameters = new HashMap<String, Object>();
			transformationParameters.put(TransformService.PARAM_ASSET_UUID, uuid);
			transformationParameters.put(TransformService.PARAM_BASE_URL, baseURL);
			String jcrXml = transformService.process(xmlFile.getAbsolutePath(), "rage2jcr2.xsl",
					transformationParameters);
			jcrFile = storageService.writeToTempFile(jcrXml);

			repository.delete("/" + uuid + "/metadata");
			repository.importJcr(jcrFile, "/" + uuid);

			// TODO update manifest file
			updateMetadataFile(uuid);

			repository.commitTransaction();
			repository.reindex("/" + uuid);
		} catch (Exception e) {
			repository.rollbackTransaction();
			throw new Exception(e.getMessage());
		} finally {
			try {
				xmlFile.delete();
				jcrFile.delete();
			} catch (Exception e) {

			}
		}
	}

	public void updateMetadataFile(UUID assetUUID) throws Exception {
		String xml = fetchXml(assetUUID);
		File xmlFile = storageService.writeToTempFile(xml);

		String canonicalXml = transformService.process(xmlFile.getAbsolutePath(), "rage2canonical.xsl",
				new HashMap<String, Object>());
		File canonicalXmlFile = storageService.writeToTempFile(canonicalXml);

		repository.updateMetadataFile(canonicalXmlFile, assetUUID);

		Path metadataFilePath = Paths.get(canonicalXmlFile.getAbsolutePath());
		Path assetZipFilePath = storageService.load(assetUUID + ".zip");
		try (FileSystem fs = FileSystems.newFileSystem(assetZipFilePath, null)) {
			Path zipMetadataFilePath = fs.getPath("/metadata.xml");
			Files.copy(metadataFilePath, zipMetadataFilePath, StandardCopyOption.REPLACE_EXISTING);
		}

		xmlFile.delete();
		canonicalXmlFile.delete();
	}

	public String upload(MultipartFile file) throws Exception {

		File jcrXmlMetadataFile = null;
		File jcrXmlFiles = null;
		String uuid = null;

		repository.beginTransaction();

		try {

			uuid = Utils.randomUUID("/");
			File assetFile = storageService.storeAssetPackage(file, uuid).toFile();

			jcrXmlFiles = storageService.createTempFile();
			processAssetPackage(assetFile, jcrXmlFiles);

			String xmlMetadataFileName = storageService.loadAssetPackageMetadata(uuid)
					.toAbsolutePath().toString();

			Map<String, Object> transformationParameters = new HashMap<String, Object>();
			String out = transformService.process(xmlMetadataFileName, "rage2jcr.xsl", transformationParameters);
			jcrXmlMetadataFile = storageService.writeToTempFile(out);

			repository.importJcr(jcrXmlMetadataFile, "");
			repository.importJcr(jcrXmlFiles, "/" + uuid);

			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			String userName = auth.getName();
			Map<String, List<String>> acl = new HashMap<String, List<String>>();
			acl.put(userName, Arrays.asList("admin"));
			acl.put("EVERYONE", Arrays.asList("reader"));
			setAccessRoles("/" + uuid, acl);

			repository.commitTransaction();
			repository.reindex("/" + uuid);

			return uuid;
		} catch (Exception e) {
			e.printStackTrace();
			repository.rollbackTransaction();
			throw new Exception(e.getMessage());
		} finally {
			try {
				jcrXmlMetadataFile.delete();
				jcrXmlFiles.delete();
				storageService.deleteAssetPackageWorkingDir(uuid);
			} catch (Exception e) {
			}
		}
	}

}
