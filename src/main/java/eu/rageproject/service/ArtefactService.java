/**
 * Copyright (C) 2016-2017 Sofia University (https://www.uni-sofia.bg/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.rageproject.service;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URI;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import org.apache.commons.lang3.EnumUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.NodeIterator;
import org.apache.jena.rdf.model.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.PathResource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import eu.rageproject.config.ArtefactSection;
import eu.rageproject.model.Artefact;
import eu.rageproject.model.fcrepo.FedoraResource;
import eu.rageproject.model.jcr.Node;
import eu.rageproject.model.jcr.NtResourceNodeHelper;
import eu.rageproject.model.xml.ArtefactType;
import eu.rageproject.model.xml.LiteralType;
import eu.rageproject.model.xml.ResourceOrLiteralType;
import eu.rageproject.repository.FedoraRepository;
import eu.rageproject.storage.FileStorageService;
import eu.rageproject.util.Utils;

@Service
public class ArtefactService {

	@Value("${multipart.maxFileSize}")
	int maxFileSize;

	@Value("${server.base.url}")
	private String baseURL;

	@Autowired
	private ResourceLoader resourceLoader;

	@Autowired
	NtResourceNodeHelper artefact;

	private final AssetService assetService;
	private final FileStorageService storageService;
	private final FedoraRepository repository;
	private final TransformService transformService;

	@Autowired
	public ArtefactService(AssetService assetService, FileStorageService storageService, FedoraRepository repository,
			TransformService transformService) {
		this.assetService = assetService;
		this.storageService = storageService;
		this.repository = repository;
		this.transformService = transformService;
	}

	public void delete(UUID assetUUID, String resourcePath) throws Exception {

		String zipFile = null;

		repository.beginTransaction();

		try {
			Model model = repository.fetchFedoraResource(resourcePath).getModel();

			Property hasFileProp = model.createProperty("http://rageproject.eu/2015/asset/hasFile");
			NodeIterator iterFile = model.listObjectsOfProperty(hasFileProp);
			if (iterFile.hasNext()) {
				String fileURI = iterFile.next().toString();
				repository.delete(new URI(fileURI));
				// check if artefact has binary file
				Property hasFileRef = model.createProperty("http://rageproject.eu/2015/asset/reference");
				NodeIterator iterRef = model.listObjectsOfProperty(hasFileRef);
				if (iterRef.hasNext()) {
					zipFile = iterRef.next().toString();
				}
			}

			repository.delete(resourcePath);

			assetService.updateMetadataFile(assetUUID);

			Path assetZipFilePath = storageService.load(assetUUID + ".zip");
			try (FileSystem fs = FileSystems.newFileSystem(assetZipFilePath, null)) {
				if (zipFile != null) {
					Path zipArtefactFilePath = fs.getPath(zipFile);
					if (Files.exists(zipArtefactFilePath)) {
						Files.delete(zipArtefactFilePath);
					}
				}
			}

			repository.commitTransaction();

		} catch (Exception e) {
			repository.rollbackTransaction();
			throw new Exception(e);
		}
	}

	public void update(UUID assetUUID, UUID artefactUUID, Artefact artefact, String path, HttpServletRequest request)
			throws Exception {

		File artefactFile = null;
		File jcrXmlMetadataFile = null;

//		FedoraResource resource = repository.fetchFedoraResource(path + "/" + artefactUUID);
//		HttpSession session = request.getSession();
//		Instant fetchedDateTime = Instant.ofEpochMilli((long) session.getAttribute("ResourceFetchedDateTime"));
//		Instant modifiedDateTime = resource.getLastModifiedDateTime();
//		if (modifiedDateTime.isAfter(fetchedDateTime)) {
//			throw new ContentConflictException("Content conflict.");
//		}

		repository.beginTransaction();

		try {
			artefactFile = storageService.writeToTempFile(artefact.getMetadata());

			repository.delete(path + "/" + artefactUUID);

			Map<String, Object> transformationParameters = new HashMap<String, Object>();
			transformationParameters.put(TransformService.PARAM_ASSET_UUID, assetUUID);
			transformationParameters.put(TransformService.PARAM_BASE_URL, baseURL);

			String jcrXml = transformService.process(artefactFile.getAbsolutePath(), "rage2jcr2.xsl",
					transformationParameters);
			jcrXmlMetadataFile = storageService.writeToTempFile(jcrXml);

			repository.importJcr(jcrXmlMetadataFile, path);
			repository.addStatement(path, "rage:hasArtefact", path + "/" + artefactUUID);
			assetService.updateMetadataFile(assetUUID);

			repository.commitTransaction();
			repository.reindex("/" + assetUUID);

		} catch (Exception e) {
			repository.rollbackTransaction();
			throw new Exception(e);
		} finally {
			try {
				artefactFile.delete();
				jcrXmlMetadataFile.delete();
			} catch (Exception e) {

			}
		}
	}

	public String fetchXml(UUID assetUUID, UUID artefactUUID, String path, HttpServletRequest request)
			throws Exception {

		FedoraResource resource = repository.fetchFedoraResource(path);
		HttpSession session = request.getSession();
		Instant modifiedDateTime = resource.getLastModifiedDateTime();
		session.setAttribute("ResourceFetchedDateTime", modifiedDateTime.toEpochMilli());

		String jcrXml = repository.exportJcr("/" + assetUUID);
		File jcrXmlFile = storageService.writeToTempFile(jcrXml);

		Map<String, Object> transformationParameters = new HashMap<String, Object>();
		transformationParameters.put(TransformService.PARAM_ARTEFACT_UUID, artefactUUID.toString());
		transformationParameters.put(TransformService.PARAM_BASE_URL, baseURL);
		String xml = transformService.process(jcrXmlFile.getAbsolutePath(), "artefact2rage.xsl",
				transformationParameters);

		try {
			jcrXmlFile.delete();
		} catch (Exception e) {

		}

		return xml;
	}

	public void create(UUID assetUUID, String section, String zipFilePath, MultipartFile file) throws Exception {

		File jcrXmlFileForBinary = null;
		File xmlFileForArtefact = null;
		File jcrXmlFileForArtefactElement = null;
		File artefactFile = null;
		
		// empty uploads
		if (file.isEmpty()) {
			throw new Exception("Rejecting empty file content");
		}

		// empty file names
		if (file.getOriginalFilename() == null || file.getOriginalFilename().isEmpty()) {
			throw new Exception("Rejecting empty file name");
		}

		if (file.getSize() > maxFileSize) {
			throw new Exception("Maximum file size is: " + maxFileSize);
		}

		// check if valid section
		if (!EnumUtils.isValidEnum(ArtefactSection.class, section)) {
			throw new Exception("Unknow section");
		}

		if (zipFilePath == null) {
			zipFilePath = "";
		}

		if (zipFilePath != null && (zipFilePath.equals("/") || zipFilePath.startsWith("/"))) {
			throw new Exception("Ivalid path parameter.");
		}

		if (zipFilePath != null && !zipFilePath.equals("") && !zipFilePath.endsWith("/")) {
			zipFilePath += "/";
		}

		Path assetZipFilePath = storageService.load(assetUUID + ".zip");
		try (FileSystem fs = FileSystems.newFileSystem(assetZipFilePath, null)) {
			Path zipArtefactFilePath = fs.getPath(zipFilePath + file.getOriginalFilename());
			if (Files.exists(zipArtefactFilePath)) {
				throw new FileAlreadyExistsException("File already exists: " + file.getOriginalFilename());
			}
		}

		repository.beginTransaction();

		try {
			String subjectURI = "/" + assetUUID + "/metadata" + ArtefactSection.valueOf(section).getParent();
			String objectURI = "/" + assetUUID + "/metadata" + ArtefactSection.valueOf(section).getParent() + "/"
					+ section;

			if (ArtefactSection.valueOf(section).getParent().equals("/solution") && !repository.exists(subjectURI)) {
				File rdfFile = resourceLoader.getResource("classpath:rdf/mixin/solution.ttl").getFile();
				PathResource rdf = new PathResource(rdfFile.toPath());
				repository.createContainer(subjectURI, rdf);
				repository.addStatement("/" + assetUUID + "/metadata", "rage:hasSolution", subjectURI);
			}

			if (!repository.exists(objectURI)) {
				File rdfFile = resourceLoader.getResource("classpath:rdf/mixin/" + section + ".ttl").getFile();
				PathResource rdf = new PathResource(rdfFile.toPath());
				repository.createContainer(objectURI, rdf);
				repository.addStatement(subjectURI, ArtefactSection.valueOf(section).getPredicat(), objectURI);
			}

			artefactFile = File.createTempFile(UUID.randomUUID().toString(), ".tmp");
			byte[] bytes = file.getBytes();
			BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(artefactFile));
			stream.write(bytes);
			stream.close();

			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			String userName = auth.getName();

			artefact.setUserName(userName);
			artefact.setFileName(file.getOriginalFilename());
			artefact.setHasSize(file.getSize());
			artefact.setMimeType(file.getContentType());
			artefact.setPath(zipFilePath + file.getOriginalFilename());
			artefact.setUuid(Utils.randomUUID(zipFilePath + file.getOriginalFilename()));
			artefact.setSha1(Utils.sha1(file));
			artefact.setBase64FilePipe(artefactFile);

			// marshal artefact file to jcr
			jcrXmlFileForBinary = File.createTempFile(UUID.randomUUID().toString(), ".tmp");
			FileOutputStream fos = new FileOutputStream(jcrXmlFileForBinary);
			JAXBContext jaxbContextForNode = JAXBContext.newInstance(Node.class);

			Marshaller jaxbMarshallerForNode = jaxbContextForNode.createMarshaller();
			jaxbMarshallerForNode.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshallerForNode.marshal(artefact.getNode(), fos);
			fos.close();

			// create rage:Artefact object
			eu.rageproject.model.xml.ObjectFactory assetFactory = new eu.rageproject.model.xml.ObjectFactory();
			ArtefactType artefactElement = assetFactory.createArtefactType();
			artefactElement.setName(file.getOriginalFilename());
			LiteralType reference = assetFactory.createLiteralType();
			reference.setValue(zipFilePath + file.getOriginalFilename());
			artefactElement.setReference(reference);
			ResourceOrLiteralType format = assetFactory.createResourceOrLiteralType();
			format.setValue(file.getContentType());
			artefactElement.setFormat(format);
			LiteralType title = assetFactory.createLiteralType();
			title.setValue(file.getOriginalFilename());
			artefactElement.addTitle(title);

			// marshal rage:Artefact to xml
			xmlFileForArtefact = File.createTempFile(UUID.randomUUID().toString(), ".tmp");
			FileOutputStream fosa = new FileOutputStream(xmlFileForArtefact);
			JAXBContext jaxbContextForArtefact = JAXBContext.newInstance(ArtefactType.class);
			Marshaller jaxbMarshallerForArtefact = jaxbContextForArtefact.createMarshaller();
			jaxbMarshallerForArtefact.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshallerForArtefact.marshal(artefactElement, fosa);
			fosa.close();

			// generate jcr for rage:Artefact
			Map<String, Object> transformationParameters = new HashMap<String, Object>();
			String artefactName = UUID.randomUUID().toString();
			transformationParameters.put(TransformService.PARAM_ARTEFACT_NAME, artefactName);
			transformationParameters.put(TransformService.PARAM_BASE_URL, baseURL);
			String out = transformService.process(xmlFileForArtefact.getAbsolutePath(), "artefact2jcr.xsl",
					transformationParameters);
			jcrXmlFileForArtefactElement = storageService.writeToTempFile(out);

			repository.importJcr(jcrXmlFileForArtefactElement,
					"/" + assetUUID + "/metadata" + ArtefactSection.valueOf(section).getParent() + "/" + section);

			repository.importJcr(jcrXmlFileForBinary, "/" + assetUUID + "/files");

			repository.addStatement(objectURI, "rage:hasArtefact", objectURI + "/" + artefactName);

			assetService.updateMetadataFile(assetUUID);

			try (FileSystem fs = FileSystems.newFileSystem(assetZipFilePath, null)) {
				Path zipArtefactFilePath = fs.getPath(zipFilePath + file.getOriginalFilename());
				if (zipArtefactFilePath.getParent() != null && Files.notExists(zipArtefactFilePath.getParent())) {
					Files.createDirectories(zipArtefactFilePath.getParent());
				}
				Files.copy(Paths.get(artefactFile.getAbsolutePath()), zipArtefactFilePath,
						StandardCopyOption.COPY_ATTRIBUTES);
			}

			repository.commitTransaction();
			repository.reindex("/" + assetUUID);

		} catch (Exception e) {
			repository.rollbackTransaction();
			throw new Exception(e);
		} finally {
			try {
				jcrXmlFileForBinary.delete();
				xmlFileForArtefact.delete();
				jcrXmlFileForArtefactElement.delete();
				artefactFile.delete();
			} catch (Exception e) {

			}
		}
	}

	public ResponseEntity<InputStreamResource> download(UUID assetUUID, UUID fileUUID, HttpServletRequest request)
			throws Exception {
		return repository.download(assetUUID, fileUUID, request);
	}
}
