/**
 * Copyright (C) 2016-2017 Sofia University (https://www.uni-sofia.bg/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.rageproject.model.fcrepo;

import java.time.Instant;

import org.apache.jena.datatypes.RDFDatatype;
import org.apache.jena.datatypes.xsd.XSDDateTime;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.NodeIterator;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.joda.time.DateTime;

public class FedoraResource {
	private static final String FEDORA_LASTMODIFIED = "http://fedora.info/definitions/v4/repository#lastModified";
	private static final String FEDORA_CREATED = "http://fedora.info/definitions/v4/repository#created";
	private static final long NO_TIME = 0L;
	private Model model;

	public Model getModel() {
		return model;
	}

	public void setModel(Model model) {
		this.model = model;
	}

	public FedoraResource(Model model) throws Exception {
		this.model = model;
	}

	public Instant getLastModifiedDateTime() {
		final Instant createdDate = getCreatedDateTime();
		try {
			final long created = createdDate == null ? NO_TIME : createdDate.toEpochMilli();
			if (hasProperty(FEDORA_LASTMODIFIED)) {
				return Instant.ofEpochMilli(getTimestamp(FEDORA_LASTMODIFIED, created));
			}
		} catch (final Exception e) {

		}

		return null;
	}

	public Instant getCreatedDateTime() {
		try {
			if (hasProperty(FEDORA_CREATED)) {
				return Instant.ofEpochMilli(getTimestamp(FEDORA_CREATED, NO_TIME));
			}
		} catch (final Exception e) {

		}

		return null;
	}

	public boolean hasProperty(final String relPath) throws Exception {
		try {
			Property hasProperty = model.createProperty(relPath);
			return model.listObjectsOfProperty(hasProperty).hasNext();
		} catch (final Exception e) {
			throw new Exception(e);
		}
	}

	protected RDFNode getProperty(final String relPath) throws Exception {
		try {
			Property hasProperty = model.createProperty(relPath);
			NodeIterator iter = model.listObjectsOfProperty(hasProperty);
			if (iter.hasNext()) {
				return iter.next();
			}
		} catch (final Exception e) {
			throw new Exception(e);
		}
		return null;
	}

	private long getTimestamp(final String property, final long created) throws Exception {

		long timestamp = NO_TIME;
		RDFNode node = getProperty(property);

		RDFDatatype type = ((Literal) node).getDatatype();

		Object value = ((Literal) node).getValue();
		if (type != null && value != null && value instanceof XSDDateTime) {
			timestamp = new DateTime(((XSDDateTime) value).asCalendar()).getMillis();
		}

		if (timestamp < created && created > NO_TIME) {
			return created;
		}
		return timestamp;

	}

}
