/**
 * Copyright (C) 2016-2017 Sofia University (https://www.uni-sofia.bg/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.rageproject.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.Base64;
import java.util.Formatter;
import java.util.HashMap;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
public class Utils {

	private static UUIDContainer uuidContainer;
	private static ManifestContainer manifestContainer;

	public static HashMap<String, String> getUUIDContainer() {
		return uuidContainer.getData();
	}

	public static HashMap<String, String> getManifestContainer() {
		return manifestContainer.getData();
	}
	
	@Autowired
	public Utils(UUIDContainer uuidContainer, ManifestContainer manifestContainer) {
		Utils.uuidContainer = uuidContainer;
		Utils.manifestContainer = manifestContainer;
	}

	public static String randomUUID(String path) {
		String uuid = UUID.randomUUID().toString();
		uuidContainer.add(path, uuid);
		return uuid;
	}
	
	public static void addToManifest(String name, String value) {
		Utils.manifestContainer.add(name, value);
	}
	
	public static OffsetDateTime currentDateTime() {
		OffsetDateTime currentDateTime = OffsetDateTime.now();
		return currentDateTime;
	}

	public static String sha1(final MultipartFile file) throws NoSuchAlgorithmException, IOException {
		final MessageDigest messageDigest = MessageDigest.getInstance("SHA1");

		try (InputStream is = new BufferedInputStream(file.getInputStream())) {
			final byte[] buffer = new byte[1024];
			for (int read = 0; (read = is.read(buffer)) != -1;) {
				messageDigest.update(buffer, 0, read);
			}
		}

		try (Formatter formatter = new Formatter()) {
			for (final byte b : messageDigest.digest()) {
				formatter.format("%02x", b);
			}
			return formatter.toString();
		}
	}

	public static String sha1(final File file) throws NoSuchAlgorithmException, IOException {
		final MessageDigest messageDigest = MessageDigest.getInstance("SHA1");

		try (InputStream is = new BufferedInputStream(new FileInputStream(file))) {
			final byte[] buffer = new byte[1024];
			for (int read = 0; (read = is.read(buffer)) != -1;) {
				messageDigest.update(buffer, 0, read);
			}
		}

		try (Formatter formatter = new Formatter()) {
			for (final byte b : messageDigest.digest()) {
				formatter.format("%02x", b);
			}
			return formatter.toString();
		}
	}

	public static void encode64(File input, OutputStream output) throws IOException {
		FileInputStream inputStream = new FileInputStream(input);
		OutputStream encodedStream = Base64.getEncoder().wrap(output);

		byte buff[] = new byte[3000];
		int r = 0;
		while ((r = inputStream.read(buff)) > 0) {
			byte[] realBuff = Arrays.copyOf(buff, r);
			encodedStream.write(realBuff);
		}

		inputStream.close();
		encodedStream.close();
	}

	public static String getUUID(String path) throws Exception {
		String uuid = getUUIDContainer().get(path);
		if (uuid != null) {
			return uuid;
		}
		throw new Exception(String.format("Invalid path reference in artefact %s", path));
	}

	public static void copyFileUsingStream(File source, File dest) throws IOException {
	    InputStream is = null;
	    OutputStream os = null;
	    try {
	        is = new FileInputStream(source);
	        os = new FileOutputStream(dest);
	        byte[] buffer = new byte[1024];
	        int length;
	        while ((length = is.read(buffer)) > 0) {
	            os.write(buffer, 0, length);
	        }
	    } finally {
	        is.close();
	        os.close();
	    }
	}	
	
	public static String sha1(String str)
	{
	    String sha1 = "";
	    try
	    {
	        MessageDigest crypt = MessageDigest.getInstance("SHA-1");
	        crypt.reset();
	        crypt.update(str.getBytes("UTF-8"));
	        sha1 = byteToHex(crypt.digest());
	    }
	    catch(Exception e)
	    {
	        e.printStackTrace();
	    }

	    return sha1;
	}	
	
	private static String byteToHex(final byte[] hash)
	{
	    Formatter formatter = new Formatter();
	    for (byte b : hash)
	    {
	        formatter.format("%02x", b);
	    }
	    String result = formatter.toString();
	    formatter.close();
	    return result;
	}
	
	public static boolean isValidURI(String value) {
		try {
			URL u = new URL(value);
			u.toURI(); 
			return true;
		} catch (Exception e) {
			return false;
		}
	}

}
