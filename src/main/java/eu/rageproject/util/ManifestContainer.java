/**
 * Copyright (C) 2016-2017 Sofia University (https://www.uni-sofia.bg/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.rageproject.util;

import java.util.HashMap;

public class ManifestContainer {

	private HashMap<String, String> data;

    public ManifestContainer() {   
		data = new HashMap<String, String>();
    }
    
    public void add(String name, String value) {
    	if(name != null && !name.isEmpty() && value != null && !value.isEmpty()) {
    		data.put(name, value);
    	}
    }
    
    public HashMap<String, String> getData() {
    	return data;
    }
    
}