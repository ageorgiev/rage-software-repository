/**
 * Copyright (C) 2016-2017 Sofia University (https://www.uni-sofia.bg/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.rageproject.repository;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.RDFDataMgr;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.PathResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.util.UriComponentsBuilder;

import eu.rageproject.config.HttpMethods;
import eu.rageproject.model.fcrepo.FedoraResource;
import eu.rageproject.service.CasService;

@Component
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class FedoraRepository {
	private String url;
	private String txUrl;
	private String container;
	private String reindex;

	@Autowired
	CasService casService;

	@Autowired
	public FedoraRepository(FedoraRepositoryProperties properties) {
		this.url = properties.getUrl();
		this.container = properties.getContainer();
		this.reindex = properties.getReindex();
	}

	private String getContainer() {
		return container;
	}

	private String getTxUrl() {
		return txUrl;
	}

	private void setTxUrl(String url) throws Exception {
		if (url == null) {
			throw new Exception();
		}
		this.txUrl = url;
	}

	private String getBaseURL() {
		if (txUrl != null) {
			return txUrl;
		}
		return url;
	}

	public void importJcr(File file, String resourcePath) throws Exception {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.valueOf("application/xml"));
		headers.setAccept(Arrays.asList(MediaType.valueOf("application/json")));

		String uri = (new StringBuffer()).append(getBaseURL()).append("/").append(getContainer()).append(resourcePath)
				.append("/fcr:import?format=jcr/xml").toString();

		PathResource filePath = new PathResource(file.toPath());
		HttpEntity<?> entity = new HttpEntity<>(filePath, headers);

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = restTemplate.exchange(casService.addTicket(uri), HttpMethod.POST, entity,
				String.class);
		if (response.getStatusCode() != HttpStatus.CREATED) {
			throw new RepositoryException(response.getBody());
		}
	}

	public String exportJcr(String resourcePath) throws Exception {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.valueOf("application/xml"));

		String uri = (new StringBuffer()).append(getBaseURL()).append("/").append(getContainer()).append(resourcePath)
				.append("/fcr:export?format=jcr/xml&recurse=true").toString();

		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));

		return restTemplate.getForObject(casService.addTicket(uri), String.class);
	}

	public void beginTransaction() throws Exception {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = restTemplate.postForEntity(casService.addTicket(url + "/fcr:tx"), null,
				String.class);

		if (response.getStatusCode() != HttpStatus.CREATED) {
			throw new RepositoryException(response.getBody());
		}

		this.setTxUrl(response.getHeaders().getLocation().toString());
	}

	public void commitTransaction() throws Exception {
		String url = getTxUrl() + "/fcr:tx/fcr:commit";

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = restTemplate.postForEntity(casService.addTicket(url), null, String.class);

		if (response.getStatusCode() != HttpStatus.NO_CONTENT) {
			throw new RepositoryException(response.getBody());
		}
	}

	public void rollbackTransaction() throws Exception {
		String url = getTxUrl() + "/fcr:tx/fcr:rollback";

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = restTemplate.postForEntity(casService.addTicket(url), null, String.class);

		if (response.getStatusCode() != HttpStatus.NO_CONTENT) {
			throw new RepositoryException(response.getBody());
		}
	}

	// https://rage-repo.it.fmi.uni-sofia.bg:8443/fcrepo-message-consumer/reindex?baseURI=https://rage-repo.it.fmi.uni-sofia.bg:8443/fcrepo/rest/assets/c1a9502c-0639-4717-a823-f825a8f64f2e&recursive=true
	public void reindex(String resourcePath) {
		try {
			URI baseURI = UriComponentsBuilder.fromUriString(
					(new StringBuffer()).append(url).append("/").append(getContainer()).append(resourcePath).toString())
					.build().toUri();
			String url = UriComponentsBuilder.fromUriString(reindex).queryParam("baseURI", baseURI)
					.queryParam("recursive", "true").build().toUriString();

			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<String> response = restTemplate.postForEntity(casService.addTicket(url), null, String.class);

			if (response.getStatusCode() != HttpStatus.OK) {
			}
		} catch (Exception ex) {
		}
	}

	public void delete(URI uri) throws Exception {
		URI baseURI = UriComponentsBuilder
				.fromUriString((new StringBuffer()).append(getBaseURL()).append("/").append(getContainer()).toString())
				.build().toUri();
		String resourcePath = baseURI.relativize(uri).getPath();
		delete("/" + resourcePath);
	}

	public void delete(String resourcePath) throws Exception {
		String resourceURI = (new StringBuffer()).append(getBaseURL()).append("/").append(getContainer())
				.append(resourcePath).toString();
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.delete(casService.addTicket(resourceURI));
		restTemplate.delete(casService.addTicket(resourceURI + "/fcr:tombstone"));
	}

	public void addStatement(String subjectPath, String predicate, String objectPath) throws Exception {
		String subjectURI = (new StringBuffer()).append(getBaseURL()).append("/").append(getContainer())
				.append(subjectPath).toString();
		String objectURI = (new StringBuffer()).append(getBaseURL()).append("/").append(getContainer())
				.append(objectPath).toString();
		String ns = "PREFIX rage: <http://rageproject.eu/2015/asset/>\r\n";
		StringBuilder query = (new StringBuilder(ns)).append("INSERT { \r\n");
		query.append("<> ").append(predicate).append(" <").append(objectURI).append("> . \r\n");
		query.append("} \r\n");
		query.append("WHERE {} \r\n");

		RestTemplate rest = new RestTemplate(new HttpComponentsClientHttpRequestFactory());

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.valueOf("application/sparql-update"));
		final HttpEntity<String> entity = new HttpEntity<>(query.toString(), headers);
		ResponseEntity<String> response = rest.exchange(casService.addTicket(subjectURI), HttpMethod.PATCH, entity,
				String.class);

		if (response.getStatusCode() != HttpStatus.NO_CONTENT) {
			throw new RepositoryException(response.getBody());
		}
	}

	public boolean exists(String resourcePath) {
		try {
			String resourceURI = (new StringBuffer()).append(getBaseURL()).append("/").append(getContainer())
					.append(resourcePath).toString();
			RestTemplate restTemplate = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			HttpEntity<String> entity = new HttpEntity<String>(headers);
			ResponseEntity<String> response = restTemplate.exchange(casService.addTicket(resourceURI), HttpMethod.HEAD,
					entity, String.class);

			if (response.getStatusCode() == HttpStatus.OK) {
				return true;
			}
		} catch (Exception ex) {
			return false;
		}
		return false;
	}

	public void createContainer(String basePath, PathResource rdf) throws Exception {
		String baseURI = (new StringBuffer()).append(getBaseURL()).append("/").append(getContainer()).append(basePath)
				.toString();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.valueOf("text/turtle"));

		final HttpEntity<?> entity = new HttpEntity<>(rdf, headers);

		RestTemplate restTemplate = new RestTemplate(new HttpComponentsClientHttpRequestFactory());
		ResponseEntity<String> response = restTemplate.exchange(casService.addTicket(baseURI), HttpMethod.PUT, entity,
				String.class);

		if (response.getStatusCode() != HttpStatus.CREATED) {
			throw new RepositoryException(response.getBody());
		}
	}

	public FedoraResource fetchFedoraResource(String resourcePath) throws Exception {
		String resourceURI = (new StringBuffer()).append(getBaseURL()).append("/").append(getContainer())
				.append(resourcePath).toString();
		Model model = ModelFactory.createDefaultModel();
		RDFDataMgr.read(model, casService.addTicket(resourceURI));
		return new FedoraResource(model);
	}

	public void updateMetadataFile(File file, UUID uuid) throws Exception {
		String resourceURI = (new StringBuffer()).append(getBaseURL()).append("/").append(getContainer()).append("/")
				.append(uuid).append("/files/metadata").toString();
		PathResource pathResource = new PathResource(file.toPath());

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.valueOf("application/xml"));
		HttpEntity<?> entity = new HttpEntity<>(pathResource, headers);

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = restTemplate.exchange(casService.addTicket(resourceURI), HttpMethod.PUT,
				entity, String.class);
		if (response.getStatusCode() != HttpStatus.NO_CONTENT) {
			throw new RepositoryException(response.getBody());
		}
	}

	public ResponseEntity<InputStreamResource> download(UUID assetUUID, UUID fileUUID, HttpServletRequest request)
			throws Exception {

		String fileURI = (new StringBuffer()).append(getBaseURL()).append("/").append(getContainer()).append("/")
				.append(assetUUID).append("/files/").append(fileUUID).toString();

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_OCTET_STREAM));
		HttpEntity<String> entity = new HttpEntity<String>(headers);

		// TODO fix response headers
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add(new ByteArrayHttpMessageConverter());
		ResponseEntity<byte[]> response = restTemplate.exchange(casService.addTicket(fileURI),
				HttpMethod.resolve(request.getMethod()), entity, byte[].class);

		if (HttpMethod.HEAD.matches(request.getMethod())) {
			return new ResponseEntity<>(response.getHeaders(), response.getStatusCode());
		}

		return ResponseEntity.ok().headers(response.getHeaders())
				.body(new InputStreamResource(new ByteArrayInputStream(response.getBody())));
	}

	public void copy(String sourcePath, String destPath) throws Exception {

		String sourceURI = (new StringBuffer()).append(getBaseURL()).append("/").append(getContainer())
				.append(sourcePath).toString();
		String destURI = (new StringBuffer()).append(getBaseURL()).append("/").append(getContainer()).append(destPath)
				.toString();

		HttpRequestBase request = new HttpMethods.HttpCopy(new URI(casService.addTicket(sourceURI)));
		request.addHeader("Destination", destURI);
		CloseableHttpClient httpclient = HttpClients.createDefault();
		CloseableHttpResponse response = httpclient.execute(request);
		if (response.getStatusLine().getStatusCode() != 201) {
			throw new RepositoryException("Conflict.");
		}
	}

	public void query(String resourcePath, String query) throws Exception {
		String resourceURI = (new StringBuffer()).append(getBaseURL()).append("/").append(getContainer())
				.append(resourcePath).toString();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.valueOf("application/sparql-update"));
		final HttpEntity<String> requestDeleteEntity = new HttpEntity<>(query, headers);
		RestTemplate restPatchTemplate = new RestTemplate(new HttpComponentsClientHttpRequestFactory());
		ResponseEntity<String> response = restPatchTemplate.exchange(casService.addTicket(resourceURI),
				HttpMethod.PATCH, requestDeleteEntity, String.class);
		if (response.getStatusCode() != HttpStatus.NO_CONTENT) {
			throw new RepositoryException(response.getBody());
		}
	}

	public void setAccessRoles(String resourcePath, Map<String, List<String>> data) throws Exception {
		String resourceURI = (new StringBuffer()).append(getBaseURL()).append("/").append(getContainer())
				.append(resourcePath).toString();
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = restTemplate
				.postForEntity(casService.addTicket(resourceURI + "/fcr:accessroles"), data, String.class);
		if (response.getStatusCode() != HttpStatus.CREATED) {
			throw new RepositoryException(response.getBody());
		}
	}

	public String getAccessRoles(String resourcePath) throws Exception {
		String resourceURI = (new StringBuffer()).append(getBaseURL()).append("/").append(getContainer())
				.append(resourcePath).append("/fcr:accessroles").toString();
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = restTemplate.getForEntity(casService.addTicket(resourceURI), String.class);
		if (response.getStatusCode() != HttpStatus.OK) {
			throw new RepositoryException(response.getBody());
		}
		return response.getBody();
	}

}
