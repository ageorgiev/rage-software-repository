/**
 * Copyright (C) 2016-2017 Sofia University (https://www.uni-sofia.bg/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.rageproject.endpoint;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import eu.rageproject.repository.FedoraRepository;
import eu.rageproject.service.AssetService;

@RestController
@CrossOrigin
@RequestMapping("/assets")
public class AclController {
	
	private final AssetService assetService;
	private final FedoraRepository repository;
	
	@Autowired
	public AclController(AssetService assetService, FedoraRepository repository) {
		this.assetService = assetService;
		this.repository = repository;
	}
	
	@RequestMapping(value = "/{uuid}/accessroles", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public String getAccessRoles(@PathVariable String uuid) throws Exception {
		return repository.getAccessRoles("/" + uuid);
	}
	
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/ping", method = RequestMethod.GET) 
    public void ping() {
    	// keep alive session method
    }
	
	@RequestMapping(value = "/{uuid}/accessroles", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void setAccessRoles(@PathVariable String uuid, @RequestBody Map<String, List<String>> data)
			throws Exception {
		repository.beginTransaction();
		try {
			assetService.setAccessRoles("/" + uuid, data);
			repository.commitTransaction();
			repository.reindex("/" + uuid);
		} catch (Exception ex) {
			repository.rollbackTransaction();
			throw new Exception(ex.getMessage());
		}
	}
}
