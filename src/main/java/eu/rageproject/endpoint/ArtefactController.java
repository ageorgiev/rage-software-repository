/**
 * Copyright (C) 2016-2017 Sofia University (https://www.uni-sofia.bg/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.rageproject.endpoint;

import java.nio.file.FileAlreadyExistsException;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import eu.rageproject.exception.InvalidMetadataException;
import eu.rageproject.model.Artefact;
import eu.rageproject.service.ArtefactService;
import eu.rageproject.service.CasService;
import eu.rageproject.service.ContentConflictException;
import eu.rageproject.validation.ArtefactValidator;

@RestController
@CrossOrigin
@RequestMapping("/assets")
public class ArtefactController {

	@Autowired
	private ArtefactValidator artefactValidator;
	
	private final ArtefactService artefactService;

	@Autowired
	public ArtefactController(ArtefactService artefactService, CasService casService) {
		this.artefactService = artefactService;
	}

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.setValidator(artefactValidator);
	}	
	
	@RequestMapping(value = "/{assetUUID}/files/{fileUUID}", method = { RequestMethod.GET, RequestMethod.HEAD })
	public ResponseEntity<InputStreamResource> download(@PathVariable UUID assetUUID, @PathVariable UUID fileUUID,
			HttpServletRequest request) throws Exception {
		return artefactService.download(assetUUID, fileUUID, request);
	}

	@RequestMapping(value = "/{assetUUID}/artefacts", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	public void create(@PathVariable UUID assetUUID, @RequestParam("section") String section,
			@RequestParam(name = "path", required = false) String zipFilePath, @RequestParam("file") MultipartFile file)
			throws Exception {
		artefactService.create(assetUUID, section, zipFilePath, file);
	}

	@RequestMapping(value = "/{assetUUID}/metadata/solution/implementation/{artefactUUID}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteArtefactInImplementation(@PathVariable UUID assetUUID, @PathVariable UUID artefactUUID)
			throws Exception {
		String path = "/" + assetUUID + "/metadata/solution/implementation/" + artefactUUID;
		artefactService.delete(assetUUID, path);
	}

	@RequestMapping(value = "/{assetUUID}/metadata/solution/requirements/{artefactUUID}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteArtefactInRequirements(@PathVariable UUID assetUUID, @PathVariable UUID artefactUUID)
			throws Exception {
		String path = "/" + assetUUID + "/metadata/solution/requirements/" + artefactUUID;
		artefactService.delete(assetUUID, path);
	}

	@RequestMapping(value = "/{assetUUID}/metadata/solution/design/{artefactUUID}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteArtefactInDesign(@PathVariable UUID assetUUID, @PathVariable UUID artefactUUID) throws Exception {
		String path = "/" + assetUUID + "/metadata/solution/design/" + artefactUUID;
		artefactService.delete(assetUUID, path);
	}

	@RequestMapping(value = "/{assetUUID}/metadata/solution/tests/{artefactUUID}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteArtefactInTests(@PathVariable UUID assetUUID, @PathVariable UUID artefactUUID) throws Exception {
		String path = "/" + assetUUID + "/metadata/solution/tests/" + artefactUUID;
		artefactService.delete(assetUUID, path);
	}

	@RequestMapping(value = "/{assetUUID}/metadata/usage/{artefactUUID}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteArtefactInUsage(@PathVariable UUID assetUUID, @PathVariable UUID artefactUUID) throws Exception {
		String path = "/" + assetUUID + "/metadata/usage/" + artefactUUID;
		artefactService.delete(assetUUID, path);
	}

	@RequestMapping(value = "/{assetUUID}/metadata/solution/implementation/{artefactUUID}/metadata", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<String> fetchArtefactMetadataInImplementation(@PathVariable UUID assetUUID,
			@PathVariable UUID artefactUUID, HttpServletRequest request) throws Exception {
		String path = "/" + assetUUID + "/metadata/solution/implementation/" + artefactUUID;
		String xml = artefactService.fetchXml(assetUUID, artefactUUID, path, request);
		return new ResponseEntity<String>(xml, HttpStatus.OK);
	}

	@RequestMapping(value = "/{assetUUID}/metadata/solution/requirements/{artefactUUID}/metadata", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<String> fetchArtefactMetadataInRequirements(@PathVariable UUID assetUUID,
			@PathVariable UUID artefactUUID, HttpServletRequest request) throws Exception {
		String path = "/" + assetUUID + "/metadata/solution/requirements/" + artefactUUID;
		String xml = artefactService.fetchXml(assetUUID, artefactUUID, path, request);
		return new ResponseEntity<String>(xml, HttpStatus.OK);
	}

	@RequestMapping(value = "/{assetUUID}/metadata/solution/design/{artefactUUID}/metadata", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<String> fetchArtefactMetadataInDesign(@PathVariable UUID assetUUID,
			@PathVariable UUID artefactUUID, HttpServletRequest request) throws Exception {
		String path = "/" + assetUUID + "/metadata/solution/design/" + artefactUUID;
		String xml = artefactService.fetchXml(assetUUID, artefactUUID, path, request);
		return new ResponseEntity<String>(xml, HttpStatus.OK);
	}

	@RequestMapping(value = "/{assetUUID}/metadata/solution/tests/{artefactUUID}/metadata", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<String> fetchArtefactMetadataInTests(@PathVariable UUID assetUUID,
			@PathVariable UUID artefactUUID, HttpServletRequest request) throws Exception {
		String path = "/" + assetUUID + "/metadata/solution/tests/" + artefactUUID;
		String xml = artefactService.fetchXml(assetUUID, artefactUUID, path, request);
		return new ResponseEntity<String>(xml, HttpStatus.OK);
	}

	@RequestMapping(value = "/{assetUUID}/metadata/usage/{artefactUUID}/metadata", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<String> fetchArtefactMetadataInUsage(@PathVariable UUID assetUUID,
			@PathVariable UUID artefactUUID, HttpServletRequest request) throws Exception {
		String path = "/" + assetUUID + "/metadata/usage/" + artefactUUID;
		String xml = artefactService.fetchXml(assetUUID, artefactUUID, path, request);
		return new ResponseEntity<String>(xml, HttpStatus.OK);
	}

	@RequestMapping(value = "/{assetUUID}/metadata/solution/implementation/{artefactUUID}/metadata", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public void updateArtefactMetadataInImplementation(@PathVariable UUID assetUUID, @PathVariable UUID artefactUUID,
			@Valid @ModelAttribute("metadata") Artefact artefact, BindingResult bindingResults, HttpServletRequest request) throws Exception {
		if (bindingResults.hasErrors()) {
			throw new XmlParseException(bindingResults.getAllErrors().toString());
		}		
		String path = "/" + assetUUID + "/metadata/solution/implementation";
		artefactService.update(assetUUID, artefactUUID, artefact, path, request);
	}

	@RequestMapping(value = "/{assetUUID}/metadata/solution/requirements/{artefactUUID}/metadata", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public void updateArtefactMetadataInRequirements(@PathVariable UUID assetUUID, @PathVariable UUID artefactUUID,
			@Valid @ModelAttribute("metadata") Artefact artefact, BindingResult bindingResults, HttpServletRequest request) throws Exception {
		if (bindingResults.hasErrors()) {
			throw new XmlParseException(bindingResults.getAllErrors().toString());
		}		
		String path = "/" + assetUUID + "/metadata/solution/requirements";
		artefactService.update(assetUUID, artefactUUID, artefact, path, request);
	}

	@RequestMapping(value = "/{assetUUID}/metadata/solution/design/{artefactUUID}/metadata", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public void updateArtefactMetadataInDesign(@PathVariable UUID assetUUID, @PathVariable UUID artefactUUID,
			@Valid @ModelAttribute("metadata") Artefact artefact, BindingResult bindingResults, HttpServletRequest request) throws Exception {
		if (bindingResults.hasErrors()) {
			throw new XmlParseException(bindingResults.getAllErrors().toString());
		}		
		String path = "/" + assetUUID + "/metadata/solution/design";
		artefactService.update(assetUUID, artefactUUID, artefact, path, request);
	}

	@RequestMapping(value = "/{assetUUID}/metadata/solution/tests/{artefactUUID}/metadata", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public void updateArtefactMetadataInTests(@PathVariable UUID assetUUID, @PathVariable UUID artefactUUID,
			@Valid @ModelAttribute("metadata") Artefact artefact, BindingResult bindingResults, HttpServletRequest request) throws Exception {
		if (bindingResults.hasErrors()) {
			throw new XmlParseException(bindingResults.getAllErrors().toString());
		}		
		String path = "/" + assetUUID + "/metadata/solution/tests";
		artefactService.update(assetUUID, artefactUUID, artefact, path, request);
	}

	@RequestMapping(value = "/{assetUUID}/metadata/usage/{artefactUUID}/metadata", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public void updateArtefactMetadataInUsage(@PathVariable UUID assetUUID, @PathVariable UUID artefactUUID,
			@Valid @ModelAttribute("metadata") Artefact artefact, BindingResult bindingResult, HttpServletRequest request) throws Exception {
		if (bindingResult.hasErrors()) {
			throw new InvalidMetadataException("Invalid metadata", bindingResult);
		}		
		String path = "/" + assetUUID + "/metadata/usage";
		artefactService.update(assetUUID, artefactUUID, artefact, path, request);
	}

	@ExceptionHandler(ContentConflictException.class)
	public ResponseEntity<?> handleException(ContentConflictException e) {
		return new ResponseEntity<String>(e.getMessage(), HttpStatus.CONFLICT);
	}

	@ExceptionHandler(FileAlreadyExistsException.class)
	public ResponseEntity<?> handleException(FileAlreadyExistsException e) {
		return new ResponseEntity<String>(e.getMessage(), HttpStatus.CONFLICT);
	}
	
}