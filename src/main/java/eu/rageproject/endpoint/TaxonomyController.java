/**
 * Copyright (C) 2016-2017 Sofia University (https://www.uni-sofia.bg/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.rageproject.endpoint;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import eu.rageproject.service.SesameProperties;

@RestController
@CrossOrigin
@RequestMapping("/taxonomies")
public class TaxonomyController {

	private String sesameURL;
	private String sesameRepository;
	
	@Autowired
	public TaxonomyController(SesameProperties properties) {
		this.sesameURL = properties.getLocation();
		this.sesameRepository = properties.getRepository();
	}
		
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<String> list(@RequestHeader("Accept") String accept) {
		String url = (new StringBuffer()).append(sesameURL).append("/repositories/").append(sesameRepository)
				.toString();
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.valueOf("application/sparql-query"));
		headers.setAccept(Arrays.asList(MediaType.valueOf(accept)));
		String query = "PREFIX skos: <http://www.w3.org/2004/02/skos/core#>\r\n" + 
				"PREFIX dcterms: <http://purl.org/dc/terms/>\r\n" + 
				"PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\r\n" + 
				"PREFIX dcat: <http://www.w3.org/ns/dcat#>\r\n" +
				"\r\n" + 
				"CONSTRUCT {\r\n" + 
				"  ?uri dcterms:identifier ?id .\r\n" + 
				"  ?uri dcterms:title ?title .\r\n" + 
				"  ?uri dcat:themeTaxonomy ?s .\r\n" +
				"  ?uri dcterms:description ?description .  \r\n" + 
				"} \r\n" + 
				"WHERE { \r\n" + 
				"  GRAPH ?g {\r\n" + 
				"    ?s rdf:type skos:ConceptScheme .\r\n" + 
				"    ?s  dcterms:title ?title .\r\n" + 
				"    OPTIONAL { ?s  dcterms:description ?description . }\r\n" + 
				"  }\r\n" + 
				"  BIND (REPLACE(str(?g),'(.*[/])','') AS ?id)\r\n" + 
				"    BIND (URI(CONCAT('"+linkTo(TaxonomyController.class).toString()+"', '/', ?id)) AS ?uri)\r\n" + 
				"  }\r\n" + 
				"ORDER BY ?g";
		
		HttpEntity<String> entity = new HttpEntity<String>(query, headers);
		
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);

		return new ResponseEntity<String>(response.getBody(), response.getStatusCode());
	}
	
	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public ResponseEntity<String> readByURI(@RequestParam("themeTaxonomy") String themeTaxonomy, @RequestHeader("Accept") String accept) {
		String url = (new StringBuffer()).append(sesameURL).append("/repositories/").append(sesameRepository)
				.toString();
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.valueOf("application/sparql-query"));
		headers.setAccept(Arrays.asList(MediaType.valueOf(accept)));
		String query = "PREFIX rdf:	<http://www.w3.org/1999/02/22-rdf-syntax-ns#> \r\n" + 
				"PREFIX skos: <http://www.w3.org/2004/02/skos/core#>\r\n" + 
				"PREFIX dcterms: <http://purl.org/dc/terms/>\r\n" + 
				"\r\n" + 
				"CONSTRUCT {?s ?p ?o}\r\n" + 
				"WHERE {\r\n" + 
				"  GRAPH ?g {\r\n" + 
				"    <" + themeTaxonomy + "> a skos:ConceptScheme .\r\n" + 
				"    ?s ?p ?o\r\n" + 
				"  } \r\n" + 
				"}";
		HttpEntity<String> entity = new HttpEntity<String>(query, headers);
		
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);

		return new ResponseEntity<String>(response.getBody(), response.getStatusCode());
	}	
	
	@RequestMapping(value = "/{taxonomyId}", method = RequestMethod.GET)
	public ResponseEntity<String> read(@PathVariable String taxonomyId, @RequestHeader("Accept") String accept) {		
		String url = (new StringBuffer()).append(sesameURL).append("/repositories/").append(sesameRepository)
				.toString();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.valueOf("application/sparql-query"));
		headers.setAccept(Arrays.asList(MediaType.valueOf(accept)));
		String query = "PREFIX skos: <http://www.w3.org/2004/02/skos/core#>\r\n" + 
				"PREFIX dcterms: <http://purl.org/dc/terms/>\r\n" + 
				"PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\r\n" + 
				"\r\n" + 
				"CONSTRUCT {?s ?p ?o } \r\n" + 
				"FROM <" + url + "/rdf-graphs/" + taxonomyId + "> \r\n" + 
				"WHERE {?s ?p ?o}";
		
		HttpEntity<String> entity = new HttpEntity<String>(query, headers);
		
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);

		return new ResponseEntity<String>(response.getBody(), response.getStatusCode());
	}

	@RequestMapping(value = "/{taxonomyId}", method = RequestMethod.POST)
	public ResponseEntity<String> create(@PathVariable String taxonomyId,
			@RequestHeader(value = "Content-Type", defaultValue = "text/turtle") String contentType,
			@RequestHeader @RequestBody String body) {
		return save(taxonomyId, contentType, HttpMethod.PUT, body);
	}

	@RequestMapping(value = "/{taxonomyId}", method = RequestMethod.PUT)
	public ResponseEntity<String> update(@PathVariable String taxonomyId,
			@RequestHeader(value = "Content-Type", defaultValue = "text/turtle") String contentType,
			@RequestHeader @RequestBody String body) {
		return save(taxonomyId, contentType, HttpMethod.POST, body);
	}

	private ResponseEntity<String> save(String taxonomyId, String contentType, HttpMethod method, String body) {
		String url = (new StringBuffer()).append(sesameURL).append("/repositories/").append(sesameRepository)
				.append("/rdf-graphs/").append(taxonomyId).toString();
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.valueOf(contentType));
		HttpEntity<String> entity = new HttpEntity<String>(body, headers);
		
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = restTemplate.exchange(url, method, entity, String.class);

		return new ResponseEntity<String>(response.getBody(), response.getStatusCode());
	}

	@RequestMapping(value = "/{taxonomyId}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable String taxonomyId) {
		String url = (new StringBuffer()).append(sesameURL).append("/repositories/").append(sesameRepository)
				.append("/rdf-graphs/").append(taxonomyId).toString();
		
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.delete(url);
	}
}