#Introduction
The RAGE Software Asset Repository is at the core of the asset development infrastructure and the main function of the repository is to facilitate the process of RAGE Asset development, sharing, exchange, reuse and repurposing.
##Objectives
The main objective is to develop a back-end web-based infrastructure needed for defining the whole package of attributes that make up an asset, including software, data formats, metadata, documentation, and instructional materials. This web-based infrastructure is called also RAGE Asset Repository and RAGE software library � both concepts are used as synonyms. 
#Acknowledgements
Funding for this work was provided by the EC H2020 project RAGE (Realising and Applied Gameing Eco-System) [http://www.rageproject.eu/](http://www.rageproject.eu/) Grant agreement No 644187